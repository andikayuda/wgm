 <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'PagesController@home')->name('home');
Route::get('/prize', 'PagesController@prize')->name('prize');
// Route::get('/redeem', 'PagesController@redeem')->name('redeem');
Route::get('/redeem-sub', 'PagesController@redeemSub')->name('redeemSub');
Route::get('/ads/{slug}', 'PagesController@ads')->name('ads');
Route::post('/lacak', 'PagesController@lacak')->name('lacak');
Route::post('/redeem/tracking','PagesController@redeemTracking')->name('redeemTracking');
Route::post('/redeem-form', 'PagesController@redeemForm')->name('redeemForm');
Route::get('/redeem/tracker/{kode}', 'PagesController@redeemTracker')->name('redeemTracker');
Route::get('/koin-gatotkaca', 'PagesController@koin')->name('koin');
Route::get('/cart', 'PagesController@cart')->name('cart');
Route::get('/addcart/{slug}', 'processController@addToCart')->name('addToCart');
Route::get('/remove/{id}', 'processController@destroy')->name('removeFromCart');
Route::get('/addqty/{id}', 'processController@addQty')->name('addQtyCart');
Route::get('/subqty/{id}', 'processController@subQty')->name('subQtyCart');
Route::get('/checkout','processController@store')->name('storeCart');
Route::get('/game/prize/{id}','PagesController@gamePrize')->name('gamePrize');
Route::get('/game/prize2/{id}','PagesController@gamePrizeBottom')->name('gamePrizeBottom');

Route::get('/promo/giveaway/ultra-premium/maret','UndianController@giveawayUpMaret')->name('undianGiveawayUpMaret');
Route::get('/ajax/promo/giveaway/ultra-premium/maret','UndianController@giveawayUpMaretAjax')->name('undianGiveawayUpMaretAjax');
Route::get('/lab/promo/giveaway/ultra-premium/maret','UndianController@lab_terdekat')->name('undianGiveawayUpMaretLab');

Route::get('/promo/giveaway/gebyar-giveaway-mei-2021','UndianGebyarGiveaway@lab')->name('undianGebyarGiveaway');
Route::get('/ajax/promo/giveaway/gebyar-giveaway-mei-2021/ps5','AjaxUndianGebyar@gebyarps5');
Route::get('/ajax/promo/giveaway/gebyar-giveaway-mei-2021/iphone','AjaxUndianGebyar@gebyariphone');
Route::get('/ajax/promo/giveaway/gebyar-giveaway-mei-2021/sepeda','AjaxUndianGebyar@gebyarsepeda');
Route::get('/ajax/promo/giveaway/gebyar-giveaway-mei-2021/realme','AjaxUndianGebyar@gebyarrealme');
Route::get('/ajax/promo/giveaway/gebyar-giveaway-mei-2021/diamond','AjaxUndianGebyar@gebyardiamond');

//Route::get('/giveaway','PagesController@campaign')->name('campaignPage');
Route::get('/giveaway','UndianGebyarGiveaway@index')->name('campaignPage');
Route::post('/submit/campaign','PagesController@submitCampaign')->name('submitCampaign');
Route::get('pdf-form/{id}', 'HtmlToPDFController@index')->name('pdfForm');
Route::get('pdf', 'PagesController@pdf')->name('toPdf');

Route::get('/event/april-2021/contest/tiktok/sinde','tiktokController@getCompetition')->name('leaderboardPage');
Route::get('/event/april-2021/contest/tiktok/sinde/form','PagesController@formLeaderboard')->name('formLeaderboardPage');


Route::get('/lab/promo/giveaway/ultra-premium/maret','UndianController@labgiveawayUpMaret')->name('labundianGiveawayUpMaret');
Route::get('/lab/ajax/promo/giveaway/ultra-premium/maret','UndianController@labgiveawayUpMaretAjax')->name('labundianGiveawayUpMaretAjax');


Route::get('/lab/{id}','lab@index')->name('labIndex');

// route reseller
Route::post('/post-reseller','processController@reseller')->name('postReseller');
Route::get('admin/data/analytics', 'processController@analytics')->name('analytics');
Route::get('admin/count-data-analytics', 'processController@countDataAnalytics')->name('countDataAnalytics');
Route::get('admin/count-all-data-analytics', 'processController@coutAll')->name('coutAll');
Route::get('admin/members/{id}', 'PagesController@userdataDetail')->name('userdataDetail');
Route::get('admin/statistic-cs', 'PagesController@statisticCs')->name('statisticCs');
Route::post('admin/post-activity', 'processController@processActivity')->name('processActivity');
Route::post('admin/process-follow-up','processController@followUp')->name('processFollowUp');
Route::post('admin/process-status','processController@changeUserStatus')->name('processStatus');
Route::get('reseller-data', 'ResellerController@resellerData')->name('resellerData');

// Tiktok controller
Route::post('competition','tiktokController@storeCompetition')->name('inputCompetition');
Route::get('competition','tiktokController@getCompetition')->name('getCompetition');


// Route::get('/redeem/omaru', 'PagesController@redeemOmaru')->name('redeemOmaru');

// Landing Page to CSB 
Route::get('/punya-koin-gatotkaca', 'PagesController@landingToCSB1')->name('landingToCSB1');
Route::get('/alasan-minum-lpcb', 'PagesController@landingToCSB2')->name('landingToCSB2');

Route::get('/reseller', 'PagesController@landingReseller')->name('landingReseller');
Route::get('/reseller/faq', 'PagesController@resellerFaq')->name('resellerFaq');


// Event WGM Marketplace
Route::get('/marketplace-wgm-2022', 'EventController@marketplace')->name('eventMarketplacePage');
