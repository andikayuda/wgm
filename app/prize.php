<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class prize extends Model
{
    //
    protected $table = 'affinity';
    
    
    public static function sisaKuotaPrize($id){
        $data =  DB::connection('wgm')->table('affinity')->where('id',$id)->first();
        $kuota = $data->kuotaPenukaran;
        $totalPenukaran = DB::table('redeem')->where('id',$id)->first();
        return $totalPenukaran;
    }
}
