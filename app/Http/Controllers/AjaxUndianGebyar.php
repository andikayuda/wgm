<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AjaxUndianGebyar extends Controller
{
    public function gebyarps5(){
      $check = DB::table('undian')->where('event','gebyargiveaway-ps5')->select('kode')->first();
      $kode = $check->kode;
      $partial = str_replace("*","",$kode);
      $list = DB::connection('koin')->table('kuponUndian')->join('undian','undian.id','kuponUndian.idUndian')->where('category','Ps5')->where('kuponUndian.kodeUndian','LIKE','%'.$partial)->get();
      $hadiah = "Ps5";
      $view = view('pages.undian-gebyar-mei-2021-ajax',compact('kode','list','hadiah'))->render();
      return response()->json(['html'=>$view]);
    }

    public function gebyariphone(){
      $check = DB::table('undian')->where('event','gebyargiveaway-iphone')->select('kode')->first();
      $kode = $check->kode;
      $partial = str_replace("*","",$kode);
      $list = DB::connection('koin')->table('kuponUndian')->join('undian','undian.id','kuponUndian.idUndian')->where('category','Iphone 12 Pro')->where('kuponUndian.kodeUndian','LIKE','%'.$partial)->get();
      $hadiah = "Iphone 12 Pro";
      $view = view('pages.undian-gebyar-mei-2021-ajax',compact('kode','list','hadiah'))->render();
      return response()->json(['html'=>$view]);
    }

    public function gebyarsepeda(){
      $check = DB::table('undian')->where('event','gebyargiveaway-sepeda')->select('kode')->first();
      $kode = $check->kode;
      $partial = str_replace("*","",$kode);
      $list = DB::connection('koin')->table('kuponUndian')->join('undian','undian.id','kuponUndian.idUndian')->where('category','Sepeda')->where('kuponUndian.kodeUndian','LIKE','%'.$partial)->get();
      $hadiah = "Sepeda";

      $view = view('pages.undian-gebyar-mei-2021-ajax',compact('kode','list','hadiah'))->render();
      return response()->json(['html'=>$view]);
    }

    public function gebyarrealme(){
      $check = DB::table('undian')->where('event','gebyargiveaway-realme')->select('kode')->first();
      $kode = $check->kode;
      $partial = str_replace("*","",$kode);
      $list = DB::connection('koin')->table('kuponUndian')->join('undian','undian.id','kuponUndian.idUndian')->where('category','Hp Realme')->where('kuponUndian.kodeUndian','LIKE','%'.$partial)->get();
      $hadiah = "Hp Realme";

      $view = view('pages.undian-gebyar-mei-2021-ajax',compact('kode','list','hadiah'))->render();
      return response()->json(['html'=>$view]);
    }

    public function gebyardiamond(){
      $check = DB::table('undian')->where('event','gebyargiveaway-diamond')->select('kode')->first();
      $kode = $check->kode;
      $partial = str_replace("*","",$kode);
      $list = DB::connection('koin')->table('kuponUndian')->join('undian','undian.id','kuponUndian.idUndian')->where('category','Diamond')->where('kuponUndian.kodeUndian','LIKE','%'.$partial)->get();
      $hadiah = "Diamond";

      $view = view('pages.undian-gebyar-mei-2021-ajax',compact('kode','list','hadiah'))->render();
      return response()->json(['html'=>$view]);
    }
}
