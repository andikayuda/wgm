<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use DB;
use Alert;
use Cookie;
use App\Sender;
use CRUDBooster;
use Carbon\Carbon;
use Jenssegers\Agent\Agent;

class processController extends Controller
{
    //
    public function addToCart($slug){
        $product = DB::connection('gudang')->table('products')->where('productSlug', $slug)->first();
        $images = unserialize($product->productImages);
  
        Cart::add($product->id, $product, 1, $product->productPrice, ['image' => $images[0]]);
  
        Alert::info('Produk '.$product->productName.' Berhasil dimasukkan ke keranjang','Success')->persistent('Close');
        // dd($data);
        // $data = \SweetAlert::success('Good job!')->persistent("Close");
        // dd($data);
  
        return redirect()->back()->with('success', 'Produk Berhasil ditambahkan ke keranjang');
    }

    public function store(Request $request)
    {

      $cart = Cart::content();
      //dd($cart,Cart::count(),Cart::total());
      $totalBayar = 0;
      $jmlh_item = Cart::count();

      $time = time();
      $token = "";
      $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
      mt_srand($time);
      for($i=0;$i<6;$i++){
        $token .= $codeAlphabet[mt_rand(0,strlen($codeAlphabet)-1)];
      }
      $check_unique = DB::connection('gudang')->table('bookingan')->where('kode',$token)->count();
      if($check_unique == 0){
        $token = $token;
      }else{
        $token = time();
      }

      Cookie::queue('booking', $token, 3243200);
      date_default_timezone_set("Asia/Jakarta");
      $date = date('Y-m-d H:i:s');
      $orderExpired = date('Y-m-d H:i:s',strtotime($date.'+ 1 hours'));
      $cartExpired = date('Y-m-d H:i:s',strtotime($date.'+ 3 hours'));

      $insert_bookingan = DB::connection('gudang')->table('bookingan')->insert([
            'phone' => 'waiting',
            'nama_pemesan' => 'waiting',
            'jmlh_item' => $jmlh_item,
            'totalBayar' => $totalBayar,
            'kode' => $token,
            'platform' => 'website',
            'kurir' => 'waiting',
            'toko' => '233',
            'status' => 'keranjang',
            'status_keranjang' => 'keranjang_berisi',
            'time_status_keranjang' => date('Y-m-d H:i:s')
          ]);

      foreach(Cart::content() as $cart){

        $insert = DB::connection('gudang')->table('pesanan')->insert([
          'phone' => 'waiting',
          'booking' => $token,
          'produk_id' => $cart->name->id,
          'jumlah' => $cart->qty,
          'harga' => $cart->name->productPrice * $cart->qty,
          'modal' => $cart->name->productPrice3 * $cart->qty,
          'flag_jmlh' => 1,
          'depoid' => '233'
        ]);

      }

      Cart::destroy();

      return redirect()->to('https://api.whatsapp.com/send/?phone=62081218975517&text=Order Koin Gatotkaca kode Booking '.$token.'&app_absent=0');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($rowId)
    {

    }

    public function addQty($rowId)
    {
      $item = Cart::get($rowId);
      $qty = $item->qty;
      $newQty = $qty + 1;
      Cart::update($rowId, $newQty);
      return redirect()->back();
    }

    public function subQty($rowId)
    {
      $item = Cart::get($rowId);
      $qty = $item->qty;
      $newQty = $qty - 1;
      Cart::update($rowId, $newQty);
      return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($rowId)
    {
      Cart::remove($rowId);
      return redirect()->back();
    }
    
    public function reseller(Request $request){
        // check email
        $check = DB::table('form_reseller')->where('email', $request->email)->count();
        if($check > 0){
            return redirect()->back()->with('error', 'your email already exists');
        }
        $whatsapp = $request->whatsapp;
        $whatsapp = str_replace('-','',$whatsapp);
        $whatsapp = str_replace(' ','',$whatsapp);
        $whatsapp = str_replace(' ','',$whatsapp);
        $whatsapp = str_replace('.','',$whatsapp);
        $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
        $check_number = str_split($whatsapp);
        $new_number = "62";
    
        if($check_number[0]=='0'){
          foreach($check_number as $n => $number){
            if($n > 0){
              if($check_number[1]=='8'){
                $new_number .= $number;
              }else{
                //Alert::success('Silahkan coba lagi','Nomor Tidak Valid '.$whatsapp);
                //Return redirect()->back();
                //echo "ga bisa dibenerin nomor $isi->id <br>";
                $new_number = '-';
    
              }
            }
          }
        }else{
          if($check_number[0]=='8'){
            $new_number = "62".$whatsapp;
          }elseif($check_number[0]=='6'){
            $new_number = $whatsapp;
          }elseif($check_number[0]=='+'){
            foreach($check_number as $n => $number){
              if($n > 2){
                  $new_number .= $number;
              }
            }
          }else{
            //Alert::success('Silahkan coba lagi','Nomor Tidak Valid '.$whatsapp);
            //Return redirect()->back();
            $new_number = '-';
            //echo "ga bisa dibenerin  $isi->id <br>";
    
          }
        }
        date_default_timezone_set("Asia/Bangkok");
        $agent = new Agent();
        $platform = $agent->platform();
        $version = $agent->version($platform);
        $browser = $agent->browser();
        $versionbrowser = $agent->version($browser);
        $insert = DB::table('form_reseller')->insertGetId([
            'nama'=>$request->nama,
            'email'=>$request->email,
            'no_whatsapp'=>$new_number,
            // 'toko_online'=>$request->tokoOnline,
            'link_toko'=>$request->linkToko,
            'ip_address' => \Request::ip(),
            'device' => $agent->device(),
            'platform' => $agent->platform(),
            'version_platform' => $version,
            'browser'  => $agent->browser(),
            'version_browser' => $versionbrowser,
            'languages' => $agent->languages(),
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        // dd($insert);
        $cookie = $_COOKIE['footprints'];
        // dd($cookie);
        if($cookie != NULL){
            // $encrypter = app(\Illuminate\Contracts\Encryption\Encrypter::class);
            // $decryptedString = $encrypter->decrypt($cookie);
            $decryptedString = \Crypt::decrypt($cookie, false);
            // $cookies = explode("|",$decryptedString);
            // dd($decryptedString, $cookies);
            $visits = DB::table('visits')->where('cookie_token', $decryptedString)->update([
                'form_id' => $insert
            ]);
        }
        
        return redirect()->back()->with('success', 'Your Data Has Been Inputed');
    }
    
    public function countDataAnalytics(){
        date_default_timezone_set("Asia/Jakarta");
        $getCampaigns = DB::table('summary_analytics')->where('tipe', 1)->get();
            foreach($getCampaigns as $getCampaign){
                $campaigns = DB::table('form_reseller')
                     ->leftJoin('visits', 'visits.form_id', 'form_reseller.id')
                     ->where('visits.utm_campaign', $getCampaign->campaigns)
                     ->where('visits.updated_at', '>=', Carbon::parse($getCampaign->updated_at)->addHour())
                     ->select('visits.utm_campaign as name', DB::raw('count(*) as count'), 'visits.updated_at as update')
                    //  ->groupBy('visits.utm_campaign')
                     ->get()->sortByDesc('count')->values();
                    // dd($campaigns, Carbon::parse($getCampaign->updated_at)->addHour());
            // dd($campaigns);
                foreach($campaigns as $campaign){
                    $check = DB::table('summary_analytics')->where('campaigns', $campaign->name)->where('tipe',1)->count();
                    echo $check.'<br/>';
                    if($check == 0){
                        echo 'insert<br/>';
                        $insert = DB::table('summary_analytics')->insert([
                            'campaigns'=>$campaign->name,
                            'campaigns_total'=>$campaign->count,
                            'tipe'=>1,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ]);
                    }else{
                        echo 'update<br/>';
                        $getData = DB::table('summary_analytics')->where('campaigns', $campaign->name)->where('tipe',1)->first();
                        $update = DB::table('summary_analytics')->where('campaigns', $campaign->name)->where('tipe',1)->update([
                            'campaigns_total'=>$campaign->count + $getData->campaigns_total,
                            'updated_at'=>date('Y-m-d H:i:s')
                        ]);
                    }
                }
            }
        $getMediums = DB::table('summary_analytics')->where('tipe', 2)->get();
        foreach($getMediums as $getMedium){
            $mediums = DB::table('form_reseller')
                     ->leftJoin('visits', 'visits.form_id', 'form_reseller.id')
                     ->where('visits.utm_campaign', $getMedium->mediums)
                     ->where('visits.updated_at', '>=', Carbon::parse($getMedium->updated_at)->addHour())
                     ->select('visits.utm_medium as name', DB::raw('count(*) as count'))
                    //  ->groupBy('visits.utm_medium')
                     ->get()->sortByDesc('count')->values();
            foreach($mediums as $medium){
                $check = DB::table('summary_analytics')->where('mediums', $medium->name)->where('tipe',2)->count();
                if($check == 0){
                    $insert = DB::table('summary_analytics')->insert([
                        'mediums'=>$medium->name,
                        'mediums_total'=>$medium->count,
                        'tipe'=>2,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                    ]);
                }else{
                    $getData = DB::table('summary_analytics')->where('mediums', $medium->name)->where('tipe',2)->first();
                    $update = DB::table('summary_analytics')->where('mediums', $medium->name)->where('tipe',2)->update([
                        'mediums_total'=>$medium->count + $getData->mediums_total,
                        'updated_at'=>date('Y-m-d H:i:s')
                    ]);
                }
            }
        }
        
        $getSources = DB::table('summary_analytics')->where('tipe', 3)->get();
        foreach($getSources as $getSource){
            $sources = DB::table('form_reseller')
                     ->leftJoin('visits', 'visits.form_id', 'form_reseller.id')
                     ->where('visits.utm_campaign', $getSource->sources)
                     ->where('visits.updated_at', '>=', Carbon::parse($getSource->updated_at)->addHour())
                     ->select('visits.utm_source as name', DB::raw('count(*) as count'))
                    //  ->groupBy('visits.utm_source')
                     ->get()->sortByDesc('count')->values();
            foreach($sources as $source){
                $check = DB::table('summary_analytics')->where('sources', $source->name)->where('tipe',3)->count();
                if($check == 0){
                    $insert = DB::table('summary_analytics')->insert([
                        'sources'=>$source->name,
                        'sources_total'=>$source->count,
                        'tipe'=>3,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                    ]);
                }else{
                    $getData = DB::table('summary_analytics')->where('sources', $source->name)->where('tipe',3)->first();
                    $update = DB::table('summary_analytics')->where('sources', $source->name)->where('tipe',3)->update([
                        'sources_total'=>$source->count + $getData->sources_total,
                        'updated_at'=>date('Y-m-d H:i:s')
                    ]);
                }
            }
        }
        $getContents = DB::table('summary_analytics')->where('tipe', 4)->get();
        foreach($getContents as $getContent){
            $contents = DB::table('form_reseller')
                         ->leftJoin('visits', 'visits.form_id', 'form_reseller.id')
                         ->where('visits.utm_campaign', $getContent->contents)
                         ->where('visits.updated_at', '>=', Carbon::parse($getContent->updated_at)->addHour())
                         ->select('visits.utm_content as name', DB::raw('count(*) as count'))
                        //  ->groupBy('visits.utm_content')
                         ->get()->sortByDesc('count')->values();
            foreach($contents as $content){
                $check = DB::table('summary_analytics')->where('contents', $content->name)->where('tipe',4)->count();
                if($check == 0){
                    $insert = DB::table('summary_analytics')->insert([
                        'contents'=>$content->name,
                        'contents_total'=>$content->count,
                        'tipe'=>4,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                    ]);
                }else{
                    $getData = DB::table('summary_analytics')->where('contents', $content->name)->where('tipe',4)->first();
                    $update = DB::table('summary_analytics')->where('contents', $content->name)->where('tipe',4)->update([
                        'contents_total'=>$content->count + $getData->contents_total,
                        'updated_at'=>date('Y-m-d H:i:s')
                    ]);
                }
            } 
        }
    }
    
    public function coutAll(){
        date_default_timezone_set("Asia/Jakarta");
                $campaigns = DB::table('form_reseller')
                     ->leftJoin('visits', 'visits.form_id', 'form_reseller.id')

                     ->select('visits.utm_campaign as name', DB::raw('count(*) as count'))
                     ->groupBy('visits.utm_campaign')
                     ->get()->sortByDesc('count')->values();
            // dd($campaigns);
                foreach($campaigns as $campaign){
                    $check = DB::table('summary_analytics')->where('campaigns', $campaign->name)->where('tipe',1)->count();
                    echo $check.'<br/>';
                    if($check == 0){
                        echo 'insert<br/>';
                        $insert = DB::table('summary_analytics')->insert([
                            'campaigns'=>$campaign->name,
                            'campaigns_total'=>$campaign->count,
                            'tipe'=>1,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ]);
                    }else{
                        echo 'update<br/>';
                        $getData = DB::table('summary_analytics')->where('campaigns', $campaign->name)->where('tipe',1)->first();
                        $update = DB::table('summary_analytics')->where('campaigns', $campaign->name)->where('tipe',1)->update([
                            'campaigns_total'=>$campaign->count,
                            'updated_at'=>date('Y-m-d H:i:s')
                        ]);
                    }
                }
            

            $mediums = DB::table('form_reseller')
                     ->leftJoin('visits', 'visits.form_id', 'form_reseller.id')
                     ->select('visits.utm_medium as name', DB::raw('count(*) as count'))
                     ->groupBy('visits.utm_medium')
                     ->get()->sortByDesc('count')->values();
            foreach($mediums as $medium){
                $check = DB::table('summary_analytics')->where('mediums', $medium->name)->where('tipe',2)->count();
                if($check == 0){
                    $insert = DB::table('summary_analytics')->insert([
                        'mediums'=>$medium->name,
                        'mediums_total'=>$medium->count,
                        'tipe'=>2,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                    ]);
                }else{
                    $getData = DB::table('summary_analytics')->where('mediums', $medium->name)->where('tipe',2)->first();
                    $update = DB::table('summary_analytics')->where('mediums', $medium->name)->where('tipe',2)->update([
                        'mediums_total'=>$medium->count,
                        'updated_at'=>date('Y-m-d H:i:s')
                    ]);
                }
            }
        
        
            $sources = DB::table('form_reseller')
                     ->leftJoin('visits', 'visits.form_id', 'form_reseller.id')
                     ->select('visits.utm_source as name', DB::raw('count(*) as count'))
                     ->groupBy('visits.utm_source')
                     ->get()->sortByDesc('count')->values();
            foreach($sources as $source){
                $check = DB::table('summary_analytics')->where('sources', $source->name)->where('tipe',3)->count();
                if($check == 0){
                    $insert = DB::table('summary_analytics')->insert([
                        'sources'=>$source->name,
                        'sources_total'=>$source->count,
                        'tipe'=>3,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                    ]);
                }else{
                    $getData = DB::table('summary_analytics')->where('sources', $source->name)->where('tipe',3)->first();
                    $update = DB::table('summary_analytics')->where('sources', $source->name)->where('tipe',3)->update([
                        'sources_total'=>$source->count,
                        'updated_at'=>date('Y-m-d H:i:s')
                    ]);
                }
            }
        

            $contents = DB::table('form_reseller')
                         ->leftJoin('visits', 'visits.form_id', 'form_reseller.id')
                         ->select('visits.utm_content as name', DB::raw('count(*) as count'))
                         ->groupBy('visits.utm_content')
                         ->get()->sortByDesc('count')->values();
            foreach($contents as $content){
                $check = DB::table('summary_analytics')->where('contents', $content->name)->where('tipe',4)->count();
                if($check == 0){
                    $insert = DB::table('summary_analytics')->insert([
                        'contents'=>$content->name,
                        'contents_total'=>$content->count,
                        'tipe'=>4,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                    ]);
                }else{
                    $getData = DB::table('summary_analytics')->where('contents', $content->name)->where('tipe',4)->first();
                    $update = DB::table('summary_analytics')->where('contents', $content->name)->where('tipe',4)->update([
                        'contents_total'=>$content->count,
                        'updated_at'=>date('Y-m-d H:i:s')
                    ]);
                }
            } 
    }
    
    public function analytics(){
        $campaigns = DB::table('summary_analytics')->where('tipe', 1)->get();
        $mediums = DB::table('summary_analytics')->where('tipe', 2)->get();
        $sources = DB::table('summary_analytics')->where('tipe', 3)->get();
        $contents = DB::table('summary_analytics')->where('tipe', 4)->get();
        // dd($campaigns, $mediums, $sources, $contents);
        return view('admin.leads', compact('campaigns', 'mediums', 'sources', 'contents'));
    }
    
    public function followUp(Request $request){
        $id = DB::table('activities')->where('user_id', $request->user_id)->orderBy('id', 'desc')->first();
        if($id){
            DB::table('activities')->where('id', $id->id)->update([
            'next_fu' => $request->next_fu
            ]);
        }
        else{
            $getCsId = DB::table('customer_service')->where('cms_users_id', CRUDBooster::myID())->first();
            DB::table('activities')->insert([
            'admin_id' => $getCsId->id,
            'user_id' => $request->user_id,
            'next_fu' => $request->next_fu
            ]);
        }

        return redirect()->back();
    }
    
    public function changeUserStatus(Request $request){
        $getCsId = DB::table('customer_service')->where('cms_users_id', CRUDBooster::myID())->first();
        DB::table('form_reseller')->where('id', $request->user_id)->update([
            'status' => $request->status,
            'admin_id' => $getCsId->id
            ]);
        return redirect()->back();
    }
    
     public function processActivity(Request $request){
        date_default_timezone_set("Asia/Bangkok");
        \DB::table('activities')->insert([
            'user_id' => $request->user_id,
            'admin_id' => $request->admin_id,
            'activity' => $request->activity,
            'created_at' => now(),
            'updated_at' => now()
            ]);
        return redirect()->back();
    }
}
