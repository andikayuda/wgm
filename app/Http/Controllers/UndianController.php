<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UndianController extends Controller
{
    public function giveawayUpMaret(){
      $toko_undian = ['219','220','221','222','231','227'];
      foreach($toko_undian as $x => $toko){
        $undian = DB::connection('gudang')->table('bookingan')->where('toko',$toko_undian[$x])
                  ->where('platform','tokopedia')
                  ->whereDate('created_at','<','2021-03-08')->whereDate('created_at','>','2021-02-27')
                  ->get();
        foreach($undian as $kupon){
          $kode = substr($kupon->kode,-4);
          $giveaway[] = [
                          'kode' => $kode,
                          'invoice' => $kupon->kode,
                          'nama' => $kupon->nama_pemesan
                        ];
          //echo $n++." | ".$kupon->kode." | ".$kupon->nama_pemesan."<br>";
        }
      }

      asort($giveaway);
      return view('pages.giveaway-maret',compact('giveaway'));
    }

    public function giveawayUpMaretAjax(){
      $pemenang = DB::table('undian')->where('event','giveawayUpMaret')->first();
      $code = str_split($pemenang->kode);
      $toko_undian = ['219','220','221','222','231','227'];
      foreach($toko_undian as $x => $toko){
        $undian = DB::connection('gudang')->table('bookingan')->where('toko',$toko_undian[$x])
                  ->where('platform','tokopedia')
                  ->whereDate('created_at','<','2021-03-08')->whereDate('created_at','>','2021-02-27')
                  ->get();
        foreach($undian as $kupon){
          $kode = substr($kupon->kode,-4);

          if($kode == $pemenang->kode){
            $winner = $kupon->nama_pemesan;
          }

          if(is_numeric ($pemenang->kode)){
              $selisih = abs($kode - $pemenang->kode);
          }else{
            $selisih =0;
          }

          $giveaway[] = [
                          'kode' => $kode,
                          'invoice' => $kupon->kode,
                          'nama' => $kupon->nama_pemesan
                        ];

          $closest[] = [
                          'selisih' => $selisih,
                          'kode' => $kode,
                          'invoice' => $kupon->kode,
                          'nama' => $kupon->nama_pemesan

                        ];
        }
      }
      asort($closest);
      $view = view('pages.undian-giveaway-maret-ajax',compact('pemenang','code','winner','closest'))->render();
      return response()->json(['html'=>$view]);
    }

    public function lab_terdekat(){
      $pemenang = '0000';
      $toko_undian = ['219','220','221','222','231','227'];
      foreach($toko_undian as $x => $toko){
        $undian = DB::connection('gudang')->table('bookingan')->where('toko',$toko_undian[$x])
                  ->where('platform','tokopedia')
                  ->whereDate('created_at','<','2021-03-08')->whereDate('created_at','>','2021-02-27')
                  ->get();
        foreach($undian as $kupon){
          $kode = substr($kupon->kode,-4);

          if($kode == $pemenang->kode){
            $winner = $kupon->nama_pemesan;
          }

          $selisih = abs($kode - $pemenang);

          $giveaway[] = [
                          'kode' => $kode,
                          'invoice' => $kupon->kode,
                          'nama' => $kupon->nama_pemesan
                        ];

          $closest[] = [
                          'selisih' => $selisih,
                          'kode' => $kode,
                          'invoice' => $kupon->kode,
                          'nama' => $kupon->nama_pemesan

                        ];
          //echo $n++." | ".$kupon->kode." | ".$kupon->nama_pemesan."<br>";
        }
      }
      asort($closest);

      foreach ($closest as $key => $value) {
        dd($key,$value);
      }

    }

    public function labgiveawayUpMaret(){
      $toko_undian = ['219','220','221','222','231','227'];
      foreach($toko_undian as $x => $toko){
        $undian = DB::connection('gudang')->table('bookingan')->where('toko',$toko_undian[$x])
                  ->where('platform','tokopedia')
                  ->whereDate('created_at','<','2021-03-08')->whereDate('created_at','>','2021-02-27')
                  ->get();
        foreach($undian as $kupon){
          $kode = substr($kupon->kode,-4);
          $giveaway[] = [
                          'kode' => $kode,
                          'invoice' => $kupon->kode,
                          'nama' => $kupon->nama_pemesan
                        ];
          //echo $n++." | ".$kupon->kode." | ".$kupon->nama_pemesan."<br>";
        }
      }

      asort($giveaway);
      return view('pages.lab-giveaway-maret',compact('giveaway'));
    }

    public function labgiveawayUpMaretAjax(){
      $pemenang = DB::table('undian')->where('event','LATIHANgiveawayUpMaret')->first();
      $code = str_split($pemenang->kode);
      $toko_undian = ['219','220','221','222','231','227'];
      foreach($toko_undian as $x => $toko){
        $undian = DB::connection('gudang')->table('bookingan')->where('toko',$toko_undian[$x])
                  ->where('platform','tokopedia')
                  ->whereDate('created_at','<','2021-03-08')->whereDate('created_at','>','2021-02-27')
                  ->get();
        foreach($undian as $kupon){
          $kode = substr($kupon->kode,-4);

          if($kode == $pemenang->kode){
            $winner = $kupon->nama_pemesan;
          }

          if(is_numeric ($pemenang->kode)){
              $selisih = abs($kode - $pemenang->kode);
          }else{
            $selisih =0;
          }

          $giveaway[] = [
                          'kode' => $kode,
                          'invoice' => $kupon->kode,
                          'nama' => $kupon->nama_pemesan
                        ];

          $closest[] = [
                          'selisih' => $selisih,
                          'kode' => $kode,
                          'invoice' => $kupon->kode,
                          'nama' => $kupon->nama_pemesan

                        ];
        }
      }
      asort($closest);
      $view = view('pages.lab-undian-giveaway-maret-ajax',compact('pemenang','code','winner','closest'))->render();
      return response()->json(['html'=>$view]);
    }
}
