<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;
use DB;

class ResellerController extends Controller
{
    public function resellerData(){
        $client = new Client();
        $resData = $client->get("https://gudang.warisangajahmada.com/api/resellerData")->getBody();
        $responses = json_decode($resData);
        // dd($responses);
        foreach($responses as $response){
            $check = DB::table('pesanan_reseller')->where('kode', $response->kode)->count();
            if($check < 1){
                $insertData = DB::table('pesanan_reseller')->insertGetId([
                    'nama'=>$response->nama_pemesan,
                    'toko'=>$response->nama_ukm,
                    'alamat'=>$response->alamat,
                    'no_hp'=>$response->phone,
                    'kode'=>$response->kode,
                    'total_harga'=>$response->totalBayar
                ]);
                foreach($response->order as $index=>$product){
                    // dd($product);
                    $insert= DB::table('reseller_order_item')->insert([
                        'nama_produk'=>$product->productName,
                        'qty'=>$product->jumlah,
                        'order_id'=>$insertData,
                    ]);
                }
            }
            
        }
        return 'success';
    }
}
