<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Http\Controllers\Controller;
use App\Http\Requests;
use DB;
use PDF;

class HtmlToPDFController extends Controller
{
    public function index($id)
    {
        $id = $id;
        $usersData = DB::connection('koin')->table('undian')->where('id', $id)->first();
        $users = $usersData->nama;
        $alamat = $usersData->alamat;
        $noWhatsapp = $usersData->noWhatsapp;
        $kode_form = $usersData->kode_form;
        $jumlahKoinTotal = $usersData->jumlahKoinTotal;
        $jumlahKoinPs5 = $usersData->jumlahKoinPs5;
        $jumlahKoinHp = $usersData->jumlahKoinHp;
        $jumlahKoinIphone = $usersData->jumlahKoinIphone;
        $jumlahKoinDiamond = $usersData->jumlahKoinDiamond;
        $jumlahKoinSepeda = $usersData->jumlahKoinTotal;
        $listKupon = [
            'Iphone'=>$usersData->jumlahKoinIphone,
            'Ps5'=>$usersData->jumlahKoinPs5,
            'hp'=>$usersData->jumlahKoinHp,
            'Sepeda'=>$usersData->jumlahKoinSepeda,
            'Diamond'=>$usersData->jumlahKoinDiamond,
        ];
        // dd($id);
        // $user = DB::table("users")->get();
        // view()->share('users',$users);
        // if($request->has('download')){
        	// Set extra option
        	PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        	// pass view file
            $pdf = PDF::loadView('pdfview',['data'=>$users, 'alamat'=>$alamat, 'noWhatsapp'=>$noWhatsapp, 'kode_form'=>$kode_form, 'jumlahKoinTotal'=>$jumlahKoinTotal, 'jumlahKoinPs5'=>$jumlahKoinPs5, 'jumlahKoinHp'=>$jumlahKoinHp, 'jumlahKoinIphone'=> $jumlahKoinIphone, 'jumlahKoinDiamond'=>$jumlahKoinDiamond, 'jumlahKoinSepeda'=> $jumlahKoinSepeda, 'listKupon'=>$listKupon]);
            // download pdf
            return $pdf->download('pdfview.pdf');
        // }
        // return view('pdfview');
    }
}
