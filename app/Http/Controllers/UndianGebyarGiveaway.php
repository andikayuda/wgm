<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UndianGebyarGiveaway extends Controller
{
  public function index(){

    $categories = DB::connection('koin')->table('kuponUndian')->distinct()->select('category')->get();
    foreach ($categories as $cat) {
      $undian = DB::connection('koin')->table('kuponUndian')->join('undian','undian.id','kuponUndian.idUndian')->where('kuponUndian.category',$cat->category)->get();
      $data[] = [
        'slug' => str_slug($cat->category),
        'category' => $cat->category,
        'peserta' => $undian
      ];
    }
    return view('pages.gebyar-giveaway',compact('data'));
  }

  public function lab(){

    $categories = DB::connection('koin')->table('kuponUndian')->distinct()->select('category')->get();
    foreach ($categories as $cat) {
      $undian = DB::connection('koin')->table('kuponUndian')->join('undian','undian.id','kuponUndian.idUndian')->where('kuponUndian.category',$cat->category)->get();
      $data[] = [
        'slug' => str_slug($cat->category),
        'category' => $cat->category,
        'peserta' => $undian
      ];
    }
    return view('pages.lab-gebyar-mei-2021',compact('data'));
  }
}
