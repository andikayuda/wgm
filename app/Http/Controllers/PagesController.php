<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Redirect;
// use Illuminate\Support\Facades\URL;
use Jenssegers\Agent\Agent;
use DB;
use Cart;
use Validator;
use CRUDBooster;
use Carbon\carbon;


class PagesController extends Controller
{
    public function home()
    {
        // dd(CRUDBooster::adminPath());
        $products = DB::connection('gudang')->table('products')->where('productStatus','active')->where('productFunction', '1')->paginate(12);
        $config['content'] = "data dari tokped";
        // $config['to'] = CRUDBooster::adminPath('affinity');
        $config['to'] = "https://www.tokopedia.com/";
        // dd($config);
        $config['id_cms_users'] = [1];
        CRUDBooster::sendNotification($config);
        return view('pages.home',compact('products'));
    }
    public function prize() {
        $prizeSingle = DB::table('affinity')->get();
        $pullEachAffinity = DB::table('affinitygame')->distinct()->select('affinityId')->get();
        // dd($pullEachAffinity);
        $i=0;
        foreach ($pullEachAffinity as $item) {
          // dd($item->affinityId);
          $pullItem = DB::table('affinitygame')->where('affinityId', $item->affinityId)->get();
          foreach ($pullItem as $key => $value) {
            if($value->affinityId == 7)
            $value->tag = 'ff';
            if($value->affinityId == 8)
            $value->tag = 'ml';
            if($value->affinityId == 9)
            $value->tag = 'tokped';
            if($value->affinityId == 12)
            $value->tag = 'ewallet';
          }
          $prizeGame[$i] = collect($pullItem)->toArray();
          
          //dd($pullItem);
          $i++;
        }
        //dd($prizeGame);
        return view('pages.prize', compact('prizeSingle', 'prizeGame'));
    }

    public function redeem() {

        $dataRedeem = DB::table('affinity')->get();
        $dataRedeem2 = DB::table('affinitygame')->get();
        $agent = new Agent();
        $device = $agent->isMobile();

        $kodeRedeem = '';
        // dd($dataRedeem);
        return view('pages.redeem', compact('dataRedeem', 'device', 'dataRedeem2', 'kodeRedeem'));

    }


    public function cart(){
        // dd(Cart::Content());
        return view('pages.cart');
    }
    public function redeemForm(Request $request) {
            $request->validate([
                'pilihan' => 'required',
                'nama' => 'required',
                'hp' => 'required',
                'email' => 'required',
                'alamat' => 'required',
            ]
        );
        
        $kodeRedeem = str_random(6);
      $kodeRedeem = mb_strtoupper($kodeRedeem);

      $whatsapp = $request->phone;
    $whatsapp = str_replace('-','',$whatsapp);
    $whatsapp = str_replace(' ','',$whatsapp);
    $whatsapp = str_replace(' ','',$whatsapp);
    $whatsapp = str_replace('.','',$whatsapp);
    $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
    $check_number = str_split($whatsapp);
    $new_number = "62";

    if($check_number[0]=='0'){
      foreach($check_number as $n => $number){
        if($n > 0){
          if($check_number[1]=='8'){
            $new_number .= $number;
          }else{
            //Alert::success('Silahkan coba lagi','Nomor Tidak Valid '.$whatsapp);
            //Return redirect()->back();
            //echo "ga bisa dibenerin nomor $isi->id <br>";
            $new_number = '-';

          }
        }
      }
    }else{
      if($check_number[0]=='8'){
        $new_number = "62".$whatsapp;
      }elseif($check_number[0]=='6'){
        $new_number = $whatsapp;
      }elseif($check_number[0]=='+'){
        foreach($check_number as $n => $number){
          if($n > 2){
              $new_number .= $number;
          }
        }
      }else{
        //Alert::success('Silahkan coba lagi','Nomor Tidak Valid '.$whatsapp);
        //Return redirect()->back();
        $new_number = '-';
        //echo "ga bisa dibenerin  $isi->id <br>";

      }
    }

    $category = DB::table('affinity')->where('id', $request->pilihan)->pluck('kategori')->first();
    // dd($category);
    // single prize
    if($category == "Single") {
      $namaHadiah = DB::table('affinity')->where('id', $request->pilihan)->pluck('brand')->first();
      $data = DB::table('affinity')->where('id',$request->pilihan)->first();
      $brand = $namaHadiah;
      $kuotaPenukaran = $data->kuotaPenukaran;
    //   $totalPenukaran = DB::connection('koin')->table('redeem')->where('jenisHadiah',$brand)->where('statusKoin', 'diterima sesuai')->count();
      $lastTransaction = DB::connection('koin')->table('redeem')->orderBy('tanggalStatusRedeem', 'desc')->first();
      $timestamp= strtotime($lastTransaction->tanggalStatusRedeem);
      $lastTransaction_month = date('n', $timestamp);
      $current_Month = carbon::now()->month;
	   
    //  reset code 	 
    //   if($lastTransaction_month != $current_Month){
    //       $getAff = DB::connection('wgm')->table('affinity')->get();
    //       foreach($getAff as $item){
    //           DB::connection('wgm')->table('affinity')->where('id', $item->id)->update(['kuotaPenukaran'=> $item->kuotaAwal]);
    //       }
    //       $getAffGame = DB::connection('wgm')->table('affinitygame')->get();
    //       foreach($getAffGame as $item){
    //           DB::connection('wgm')->table('affinitygame')->where('id', $item->id)->update(['kuotaPenukaran'=> $item->kuotaAwal]);
    //       }
    //   }
      if($kuotaPenukaran > 0){
          $tukar = true;

        }
        else{
          $tukar = false;

        }
    //   dd($namaHadiah);

    }else if($category == "Game") {
      $namaHadiah = DB::table('affinitygame')->where('id', $request->tipeHadiah)->pluck('prize')->first();
      $data = DB::table('affinitygame')->where('id',$request->tipeHadiah)->first();
      $brand = $namaHadiah;
      $kuotaPenukaran = $data->kuotaPenukaran;
    //   $totalPenukaran = DB::connection('koin')->table('redeem')->where('jenisHadiah',$brand)->where('statusKoin', 'diterima sesuai')->count();
      $lastTransaction = DB::connection('koin')->table('redeem')->orderBy('tanggalStatusRedeem', 'desc')->first();
    
      $timestamp= strtotime($lastTransaction->tanggalStatusRedeem);
      $lastTransaction_month = date('n', $timestamp);
      $current_Month = carbon::now()->month;
	 //  reset code 	  
    //   if($lastTransaction_month != $current_Month){
    //       $getAff = DB::connection('wgm')->table('affinity')->get();
    //       foreach($getAff as $item){
    //           DB::connection('wgm')->table('affinity')->where('id', $item->id)->update(['kuotaPenukaran'=> $item->kuotaAwal]);
    //       }
    //       $getAffGame = DB::connection('wgm')->table('affinitygame')->get();
    //       foreach($getAffGame as $item){
    //           DB::connection('wgm')->table('affinitygame')->where('id', $item->id)->update(['kuotaPenukaran'=> $item->kuotaAwal]);
    //       }
    //   }
      
      if($kuotaPenukaran > 0){
          $tukar = true;

        }
        else{
          $tukar = false;
        //   dd($tukar);
        }
    //   dd($namaHadiah);
    }
    
    if($tukar == true){
        // return redirect()->back()->with('error', 'Batas penukaran telah habis');
          $insert = \DB::connection('koin')->table('redeem')->insertGetId([
            'kodeRedeem' => $kodeRedeem,
            'nama' => $request->nama,
            'noHandphone' => $request->hp,
            'jenisHadiah' =>$namaHadiah,
            // 'tipeHadiah' => $request->tipeHadiah,
            'alamatEmail' => $request->email,
            'noWhatsapp' => $new_number,
            'alamat' => $request->alamat,
            'tanggalStatusRedeem' => date('Y-m-d H:i:s')
          ]);
    }
    else{
        return redirect()->back()->with('error', 'Batas penukaran telah habis');
    }

      



    //   $client = new Client();
/*
      $res = $client->post(env('CHAT-API-PATH').'sendMessage?token='.env('CHAT-API-TOKEN'),[
          'form_params' => [
            "phone" => $new_number,
            "body" => "Hi $request->nama, kode redeem kamu adalah $kodeRedeem,

silahkan cek secara berkala di https://koin.warisangajahmada.com/"
          ]
        ]);
*/
    $data = NULL;
    // dd($kodeRedeem);
      // return view('pages.redeem',compact('kodeRedeem','data'));
      return redirect()->back()->with(['kodeRedeem'=> $kodeRedeem]);
        // dd($request);
    }

    public function redeemTracker($kode){
        // dd($kode);
        $data = \DB::connection('koin')->table('redeem')->where('kodeRedeem',$kode)->first();
        $dataRedeem = DB::table('affinity')->get();
        $dataRedeem2 = DB::table('affinitygame')->get();
        $agent = new Agent();
        $device = $agent->isMobile();
        // $prizes = \DB::table('prizes')->where('statusHadiah','active')->get();
        // dd($data);
        // return view('pages.redeem',compact('data','prizes'));
        $kodeRedeem = NULL;
        // dd($data);
        // return redirect()->back()->with(['kodeRedeem'=> $kodeRedeem]);
        return view('pages.redeem',compact('data','kodeRedeem','dataRedeem','dataRedeem2','device'));
    }
    
    public function ads($slug) {
        $agent = new Agent();
        // \Session::put('isMobile', $agent->isMobile());
        $device = $agent->isMobile();
        $carousel = \DB::table('affinity')->where('slug', '!=', $slug)->get();
        if($slug == "iphone-12-pro" || $slug == "ps5" || $slug == "sepeda" || $slug == "pulsa-200k") {
          $data=\DB::table('affinity')->where('slug', $slug)->first();
          return view('pages.ads',compact('data', 'device', 'carousel'));
        }
        elseif($slug == "tokopedia" || $slug == "diamond-ml" || $slug == "diamond-ff") {
          $data=\DB::table('affinity')->where('slug', $slug)->first();
          $pictures =\DB::table('affinitygame')->where('affinityId', $data->id)->get();
          // dd($device,$data, $carousel, $pictures );
          return view('pages.ads',compact('data', 'device', 'carousel', 'pictures'));
        }
        // dd($data);
        
    }

    public function redeemTracking(Request $request){
        // dd($request);
        // if(count($request->kode) == 0){
        //   dd('tes');
        // }
        return redirect()->route('redeemTracker',$request->kode);
    }

    public function koin()
    {
        return view('pages.koin');
    }
    public function gamePrize($id)
    {
      $affinity =DB::table('affinity')->where('id', $id)->first();
      if($affinity->kategori == 'Single'){
        return response()->json(['success'=>$affinity])->setStatuscode(200);
      }else{
        $prizeData =DB::table('affinitygame')->where('affinityId', $id)->get();
        return response()->json(['success'=>$prizeData])->setStatuscode(200);
      }
     

    }
    public function gamePrizeBottom($id)
    {
      
        $prizeData =DB::table('affinitygame')->where('id', $id)->first();
        // dd($prizeDat)
        return response()->json(['success'=>$prizeData])->setStatuscode(200);
      
    }


    public function campaign()
    {
      return view('campaign');
    }
    public function submitCampaign(Request $request)
    {
      // dd($request);
      
      $request->validate([
        'jumlah' => 'required|numeric',
        'nama' => 'required',
        'hp' => 'required',
        'email' => 'required',
        'alamat' => 'required',
        ]);
        $kodeForm = str_random(6);
        $kodeForm = mb_strtoupper($kodeForm);
        
        $whatsapp = $request->hp;
        $whatsapp = str_replace('-','',$whatsapp);
        $whatsapp = str_replace(' ','',$whatsapp);
        $whatsapp = str_replace(' ','',$whatsapp);
        $whatsapp = str_replace('.','',$whatsapp);
        $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
        $check_number = str_split($whatsapp);
        $new_number = "62";
        
        if($check_number[0]=='0'){
          foreach($check_number as $n => $number){
            if($n > 0){
              if($check_number[1]=='8'){
                $new_number .= $number;
              }else{
                //Alert::success('Silahkan coba lagi','Nomor Tidak Valid '.$whatsapp);
                //Return redirect()->back();
                //echo "ga bisa dibenerin nomor $isi->id <br>";
                $new_number = '-';
                
              }
            }
          }
        }else{
          if($check_number[0]=='8'){
            $new_number = "62".$whatsapp;
          }elseif($check_number[0]=='6'){
            $new_number = $whatsapp;
          }elseif($check_number[0]=='+'){
            foreach($check_number as $n => $number){
              if($n > 2){
                $new_number .= $number;
              }
            }
          }else{
            //Alert::success('Silahkan coba lagi','Nomor Tidak Valid '.$whatsapp);
            //Return redirect()->back();
            $new_number = '-';
            //echo "ga bisa dibenerin  $isi->id <br>";
            
          }
        }
        // dd($new_number);
        // return 'tes';
        date_default_timezone_set("Asia/Jakarta");
        $insert = DB::connection('koin')->table('undian')->insertGetId([
          'nama' => $request->nama,
          'noWhatsapp' => $new_number,
          'alamatEmail' => $request->email,
          'alamat' => $request->alamat,
          'jumlahKoinTotal' => $request->jumlah,
          'kode_form' =>$kodeForm,
          'jumlahKoinPs5'=> $request->koinPs5,
          'jumlahKoinIphone'=> $request->koinIphone,
          'jumlahKoinHp'=> $request->koinHp,
          'jumlahKoinDiamond'=> $request->koinDiamond,
          'jumlahKoinSepeda'=> $request->koinSepeda,
          // 'tanggalStatusRedeem' => date('Y-m-d H:i:s')
          ]);

          // $koin = [
          //   'jumlahKoinPs5'=> $request->koinPs5,
          // 'jumlahKoinIphone'=> $request->koinIphone,
          // 'jumlahKoinHp'=> $request->koinHp,
          // 'jumlahKoinDiamond'=> $request->koinDiamond,
          // 'jumlahKoinSepeda'=> $request->koinSepeda,
          // ];

          // dd($insert);

        // return Redirect::to(URL::previous().'#alamatPengiriman');
        return response()->json([
          'message' => 'success',
          'id'=> $insert,
          'nama' => $request->nama,
          'noWhatsapp' => $new_number,
          'alamatEmail' => $request->email,
          'alamat' => $request->alamat,
          'jumlahKoinTotal' => $request->jumlah,
          'kode_form' =>$kodeForm,
          'koin'=> $koin,
          'jumlahKoinPs5'=> $request->koinPs5,
          'jumlahKoinIphone'=> $request->koinIphone,
          'jumlahKoinHp'=> $request->koinHp,
          'jumlahKoinDiamond'=> $request->koinDiamond,
          'jumlahKoinSepeda'=> $request->koinSepeda,
          ])->setStatusCode(200);
    }
    public function pdf(){
      return view('pdfview');
    }
    
    public function leaderboard()
    {
      return view('pages.leaderboard');
    }

    public function formLeaderboard()
    {
      return view('pages.form-leaderboard');
    }
    
    public function redeemOmaru() {

        $dataRedeem = DB::table('affinity')->get();
        $dataRedeem2 = DB::table('affinitygame')->get();
        $agent = new Agent();
        $device = $agent->isMobile();

        $kodeRedeem = '';
        // dd($dataRedeem);
        return view('pages.redeem-omaru', compact('dataRedeem', 'device', 'dataRedeem2', 'kodeRedeem'));

    }
    
    public function landingToCSB1() {
        return view('landing-page.landing-koin');
    }
    public function landingToCSB2() {
        return view('landing-page.landing-testi');
    }
    
    public function landingReseller() {
        
        $number = DB::table('customer_service')->where('status_cs', 'active')->select('no_whatsapp')->first();
        return view('landing-page.reseller', compact('number'));
    }
    
    public function userdataDetail($id){

        $userDetail = DB::table('visits')->where('form_id', $id)->first();
        $user = DB::table('form_reseller')->where('id', $id)->first();
        $activities = DB::table('activities')->join('customer_service', 'customer_service.id', 'activities.admin_id')->where('activities.user_id',$id)->select('customer_service.name', 'activities.activity','activities.user_id','activities.created_at','activities.updated_at')->get();
        $agent = DB::table('customer_service')->get();
        $admin = DB::table('customer_service')->where('cms_users_id', CRUDBooster::myID())->first();
        return view('admin.user-detail', compact('user','userDetail','activities', 'agent', 'admin'));
    }
    
    public function statisticCs(){
        $ana = new \stdClass();
        $ana->lead = DB::table('form_reseller')->where('status', 'lead')->where('admin_id', 2)->count();
        $ana->contact = DB::table('form_reseller')->where('status', 'contact')->where('admin_id', 2)->count();
        $ana->potential = DB::table('form_reseller')->where('status', 'potential')->where('admin_id', 2)->count();
        $ana->win = DB::table('form_reseller')->where('status', 'win')->where('admin_id', 2)->count();
        $ana->lose = DB::table('form_reseller')->where('status', 'lose')->where('admin_id', 2)->count();
        $ana->register = $anaLead + $ana->contact + $ana->potential + $ana->win + $ana->lose;
        // dd($ana);
        
        $mb = new \stdClass();
        $mb->lead = DB::table('form_reseller')->where('status', 'lead')->where('admin_id', 1)->count();
        $mb->contact = DB::table('form_reseller')->where('status', 'contact')->where('admin_id', 1)->count();
        $mb->potential = DB::table('form_reseller')->where('status', 'potential')->where('admin_id', 1)->count();
        $mb->win = DB::table('form_reseller')->where('status', 'win')->where('admin_id', 1)->count();
        $mb->lose = DB::table('form_reseller')->where('status', 'lose')->where('admin_id', 1)->count();
        $mb->register = $mbLead + $mb->contact + $mb->potential + $mb->win + $mb->lose;
        
        $total = DB::table('form_reseller')->count();
        // dd($ana, $mb, $total);
        return view('admin.statistic', compact('ana', 'mb', 'total'));
    }
    
    public function resellerFaq() {
        
        return view('landing-page.reseller-faq');
    }
    
}
