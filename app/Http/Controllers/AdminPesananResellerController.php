<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminPesananResellerController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "nama";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = true;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = false;
			$this->button_edit = true;
			$this->button_delete = false;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "pesanan_reseller";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Nama","name"=>"nama"];
			$this->col[] = ["label"=>"Toko","name"=>"toko"];
			$this->col[] = ["label"=>"Alamat","name"=>"alamat"];
			$this->col[] = ["label"=>"No Hp","name"=>"no_hp"];
			$this->col[] = ["label"=>"Customer Service","name"=>"cs_id","join"=>"customer_service,name"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Customer Service','name'=>'cs_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'customer_service,name'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Customer Service','name'=>'cs_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'customer_service,name'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();
            $this->sub_module[] = ['label'=>'Order Details','path'=>'reseller_order_item','parent_columns'=>'nama,toko,total_harga','foreign_key'=>'order_id','button_color'=>'success','button_icon'=>'fa fa-bars'];


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here
	        $check = DB::table('reseller_comission')->where('order_id', $id)->first();
            if(!empty($check)){
                $month = date('m', strtotime($check->created_at));
                $year = date('Y', strtotime($check->created_at));
                $otherTotalQty = DB::table('pesanan_reseller')->where('pesanan_reseller.cs_id', $check->cs_id)->whereYear('pesanan_reseller.created_at', '=', $year)->whereMonth('pesanan_reseller.created_at', '=', $month)->join('reseller_order_item', 'pesanan_reseller.id', 'reseller_order_item.order_id')->sum('reseller_order_item.qty');
                $totalMinusCurrentQty = (int)$otherTotalQty - (int)$check->qty;
                if($otherTotalQty > 25){
                    $komisiBeforeSubstract = (int)$otherTotalQty * 10000;
                }elseif($otherTotalQty > 11){
                    $komisiBeforeSubstract = (int)$otherTotalQty * 8000;
                }elseif($otherTotalQty > 6){
                    $komisiBeforeSubstract = (int)$otherTotalQty * 6000;
                }elseif($otherTotalQty > 3){
                    $komisiBeforeSubstract = (int)$otherTotalQty * 5000;
                }elseif($otherTotalQty > 0){
                    $komisiBeforeSubstract = (int)$otherTotalQty * 4000;
                }
                
                if($totalMinusCurrentQty > 25){
                    $komisiAfterSubstract = (int)$totalMinusCurrentQty * 10000;
                }elseif($totalMinusCurrentQty > 11){
                    $komisiAfterSubstract = (int)$totalMinusCurrentQty * 8000;
                }elseif($totalMinusCurrentQty > 6){
                    $komisiAfterSubstract = (int)$totalMinusCurrentQty * 6000;
                }elseif($totalMinusCurrentQty > 3){
                     $komisiAfterSubstract = (int)$totalMinusCurrentQty * 5000;
                }elseif($totalMinusCurrentQty > 0){
                     $komisiAfterSubstract = (int)$totalMinusCurrentQty * 4000;
                }
                
                // $updateSubstractKomisi = DB::table('customer_service')->where('id', $check->cs_id)->update([
                //     'total_komisi'=> (int)$komisiAfterSubstract
                // ]);
                $komisiTotalBeforeSubstract = DB::table('customer_service')->first();
                $komisiTotalAfterSubstract = (int)$komisiTotalBeforeSubstract->total_komisi  - $komisiBeforeSubstract;
                $finalKomisi = $komisiTotalAfterSubstract + $komisiAfterSubstract;
                $substractKomisi = DB::table('customer_service')->where('id', $check->cs_id)->update([
                    'total_komisi'=> $finalKomisi,
                ]);
                $delete = DB::table('reseller_comission')->where('order_id', $id)->delete();
            }

	        $currentMonth = date('m');
    	    $currentYear = date('Y');
	    	$lastTotalQty = DB::table('pesanan_reseller')->where('pesanan_reseller.cs_id', $postdata['cs_id'])->whereYear('pesanan_reseller.created_at', '=', $currentYear)->whereMonth('pesanan_reseller.created_at', '=', $currentMonth)->join('reseller_order_item', 'pesanan_reseller.id', 'reseller_order_item.order_id')->sum('reseller_order_item.qty');
    	    $thisOrderQty = DB::table('pesanan_reseller')->where('pesanan_reseller.id', $id)->join('reseller_order_item', 'reseller_order_item.order_id', 'pesanan_reseller.id')->sum('reseller_order_item.qty');
            $totalQty = (int)$lastTotalQty + (int)$thisOrderQty; 
            // dd($totalQty);
            if($totalQty > 25){
                $lastTotalThisMonth = DB::table('reseller_comission')->where('cs_id', $postdata['cs_id'])->first();
                $lastTotalAllTime = DB::table('customer_service')->where('id', $postdata['cs_id'])->first();
                $realKomisiLastMonth = (int)$lastTotalAllTime->total_komisi - (int)$lastTotalThisMonth->total_komisi_perbulan;
                $totalKomisi = $totalQty * 10000;
                $thisOrderKomisi = (int)$thisOrderQty * 10000;
                $insertReseller_comission = DB::table('reseller_comission')->insert([
	                'cs_id'=>$postdata['cs_id'],
	                'created_at'=>date('Y-m-d H:i:s'),
	                'qty'=>$thisOrderQty,
	                'komisi'=>$thisOrderKomisi,
	                'total_qty_perbulan'=>$totalQty,
	                'total_komisi_perbulan'=>$totalKomisi,
                    'order_id'=>$id
	            ]);
	            $totalKomisiAllTime = $realKomisiLastMonth + $totalKomisi;
	            $AllTimeComission = DB::table('customer_service')->where('id', $postdata['cs_id'])->update([
                    'total_komisi'=>$totalKomisiAllTime
                ]);
            }elseif($totalQty > 11){
                $lastTotalThisMonth = DB::table('reseller_comission')->where('cs_id', $postdata['cs_id'])->first();
                $lastTotalAllTime = DB::table('customer_service')->where('id', $postdata['cs_id'])->first();
                $realKomisiLastMonth = (int)$lastTotalAllTime->total_komisi - (int)$lastTotalThisMonth->total_komisi_perbulan;
                $totalKomisi = $totalQty * 8000;
                $thisOrderKomisi = (int)$thisOrderQty * 8000;
                $insertReseller_comission = DB::table('reseller_comission')->insert([
	                'cs_id'=>$postdata['cs_id'],
	                'created_at'=>date('Y-m-d H:i:s'),
	                'qty'=>$thisOrderQty,
	                'komisi'=>$thisOrderKomisi,
	                'total_qty_perbulan'=>$totalQty,
	                'total_komisi_perbulan'=>$totalKomisi,
                    'order_id'=>$id
	            ]);
	            $totalKomisiAllTime = $realKomisiLastMonth + $totalKomisi;
	            $AllTimeComission = DB::table('customer_service')->where('id', $postdata['cs_id'])->update([
                    'total_komisi'=>$totalKomisiAllTime
                ]);
            }elseif($totalQty > 6){
                $lastTotalThisMonth = DB::table('reseller_comission')->where('cs_id', $postdata['cs_id'])->first();
                $lastTotalAllTime = DB::table('customer_service')->where('id', $postdata['cs_id'])->first();
                $realKomisiLastMonth = (int)$lastTotalAllTime->total_komisi - (int)$lastTotalThisMonth->total_komisi_perbulan;
                $totalKomisi = $totalQty * 6000;
                $thisOrderKomisi = (int)$thisOrderQty * 6000;
                $insertReseller_comission = DB::table('reseller_comission')->insert([
	                'cs_id'=>$postdata['cs_id'],
	                'created_at'=>date('Y-m-d H:i:s'),
	                'qty'=>$thisOrderQty,
	                'komisi'=>$thisOrderKomisi,
	                'total_qty_perbulan'=>$totalQty,
	                'total_komisi_perbulan'=>$totalKomisi,
                    'order_id'=>$id
	            ]);
	            $totalKomisiAllTime = $realKomisiLastMonth + $totalKomisi;
	            $AllTimeComission = DB::table('customer_service')->where('id', $postdata['cs_id'])->update([
                    'total_komisi'=>$totalKomisiAllTime
                ]);
            }elseif($totalQty > 3){
                $lastTotalThisMonth = DB::table('reseller_comission')->where('cs_id', $postdata['cs_id'])->first();
                $lastTotalAllTime = DB::table('customer_service')->where('id', $postdata['cs_id'])->first();
                $realKomisiLastMonth = (int)$lastTotalAllTime->total_komisi - (int)$lastTotalThisMonth->total_komisi_perbulan;
                $totalKomisi = $totalQty * 5000;
                $thisOrderKomisi = (int)$thisOrderQty * 5000;
                $insertReseller_comission = DB::table('reseller_comission')->insert([
	                'cs_id'=>$postdata['cs_id'],
	                'created_at'=>date('Y-m-d H:i:s'),
	                'qty'=>$thisOrderQty,
	                'komisi'=>$thisOrderKomisi,
	                'total_qty_perbulan'=>$totalQty,
	                'total_komisi_perbulan'=>$totalKomisi,
                    'order_id'=>$id
	            ]);
	            $totalKomisiAllTime = $realKomisiLastMonth + $totalKomisi;
	            $AllTimeComission = DB::table('customer_service')->where('id', $postdata['cs_id'])->update([
                    'total_komisi'=>$totalKomisiAllTime
                ]);
            }elseif($totalQty > 0){
                $lastTotalThisMonth = DB::table('reseller_comission')->where('cs_id', $postdata['cs_id'])->first();
                $lastTotalAllTime = DB::table('customer_service')->where('id', $postdata['cs_id'])->first();
                $realKomisiLastMonth = (int)$lastTotalAllTime->total_komisi - (int)$lastTotalThisMonth->total_komisi_perbulan;
                $totalKomisi = $totalQty * 4000;
                $thisOrderKomisi = (int)$thisOrderQty * 4000;
                $insertReseller_comission = DB::table('reseller_comission')->insert([
	                'cs_id'=>$postdata['cs_id'],
	                'created_at'=>date('Y-m-d H:i:s'),
	                'qty'=>$thisOrderQty,
	                'komisi'=>$thisOrderKomisi,
	                'total_qty_perbulan'=>$totalQty,
	                'total_komisi_perbulan'=>$totalKomisi,
                    'order_id'=>$id
	            ]);
	            $totalKomisiAllTime = $realKomisiLastMonth + $totalKomisi;
	            $AllTimeComission = DB::table('customer_service')->where('id', $postdata['cs_id'])->update([
                    'total_komisi'=>$totalKomisiAllTime
                ]);
            }
    
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 


	}