<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class tiktokController extends Controller
{
    //
    public function storeCompetition(Request $request){
        // dd($request);
        // return $request;
        $validator = \Validator::make($request->all(), [
            'nik' => 'required',
            'tiktokUsername' => 'required|string',
            'mainHashtag' => 'required',
            'buktiPembelian' => 'required'
        ]);
        $tiktokUsername = $request->tiktokUsername;
        $check_number = str_split($request->tiktokUsername);
        if($check_number[0]=='@'){
            array_shift($check_number);
            $request->tiktokUsername = implode($check_number);
            // dd($request->tiktokUsername);
        }

        $hashtag = $request->mainHashtag;
        $check_number = str_split($request->mainHashtag);
        if($check_number[0]=='#'){
            array_shift($check_number);
            $request->mainHashtag = implode($check_number);
            // dd($request->mainHashtag);
        }
        
        if($validator->fails()){
            // return response()->json($validator->errors(), 200);
            // return redirect()->back();
            return redirect()->route('formLeaderboardPage')->with('error', 'insert success');
        }

        //Cek username tiktok
        $username = \DB::connection('tiktok')->table('competition')->where('tiktok_username', $request->tiktokUsername)->count();
        if($username>0){
            return redirect()->route('formLeaderboardPage')->with('username', 'username sudah terdaftar');
        }

        //end cek

        //Save profile image
        $path = NULL;
        if($request->file('buktiPembelian')){
            //  return 'gambar';
            // $request->file('file');
            date_default_timezone_set("buktiPembelian/Jakarta");
            $file = $request->file('buktiPembelian');
            $uniqueFileName = str_replace(" ","",time()."-".$file->getClientOriginalName());            
            $path = $file->storeAs('photos', time().'-'.$uniqueFileName);
            
            }
            else if($request->buktiPembelian){
               
                $base64_image = $request->buktiPembelian;
                $base64_image = "data:image/png;base64,".$base64_image;
                
                if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                    $data = substr($base64_image, strpos($base64_image, ',') + 1);
                
                    $file = base64_decode($data);
                    $uniqueFileName = "profile-pict".time();            

                    \Storage::disk('local')->put("photos/".$uniqueFileName.".png", $file);
                    $path = "photos/".$uniqueFileName.".png";
              
                }
                
                
            }
        // End save profile image
        $insert = \DB::connection('tiktok')->table('competition')->insert([
            'nik' => $request->nik,
            'tiktok_username' => $request->tiktokUsername,
            'mainHashtag' => $request->mainHashtag,
            'buktiPembelian' => $path
            ]);
            return redirect()->route('formLeaderboardPage')->with('success', 'insert success');
        // return response()->json([
        //         'status' => 'success',
        //         'message' => 'Insert Success',
        //         'data' => $insert
        //    ])->setStatusCode(200);
        
    }
    public function getCompetition(){
        $datas = \DB::connection('tiktok')
        ->table('competition')
        ->leftJoin('hashtag', 'hashtag.id', 'competition.hashtagId')
        ->where('competition.status', 1)
        ->where('competition.totalPlay', '>=', 5000)
        ->orderBy('competition.totalPlay', 'desc')
        ->get();
        // dd($datas);
        $leaderboard = \DB::connection('tiktok')
        ->table('competition')
        ->leftJoin('hashtag', 'hashtag.id', 'competition.hashtagId')
        ->where('competition.status', 1)
        ->where('competition.totalPlay', '>=', 5000)
        ->orderBy('competition.totalPlay', 'desc')
        ->limit(3)
        ->get();

        return view('pages.leaderboard', compact('datas', 'leaderboard'));
    }
}
