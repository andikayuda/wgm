@extends('layouts.app')

@section('content')
    <section class="lp_wgm_hero">
        <img src="{{ asset('images/landing-page/lp-wgm-2.png') }}" alt="" style="width: 100%">
    </section>

    <section class="section-content" style="margin-top: 50px">
        <div class="container">
            <div class="lp_wgm2_heading">
                <h5>Sedia Larutan Penyegar Cap Badak setiap saat emang gak ada rugi nya deh!
                    Bisa jadi penolong untuk urusan kesehatan tenggorokanmu.
                </h5>
                
                <div class="lp_wgm_article">
                <p>Sakit tenggorokan merupakan rasa nyeri atau tidak nyaman pada kondisi tenggorokan yang kering. Kondisi ini merupakan gejala awal yang dapat menyebabkan berbagai gangguan atau penyakit dalam tubuh, salah satunya yang disebabkan oleh infeksi virus. Keluhan yang paling sering ditemui ketika mengalami sakit ini adalah sulit menelan ketika makan atau minum. Beberapa penyakit yang menyebabkan sakit pada tenggorokan antara lain, amandel, gejala batuk atau flu, panas dalam, atau bisa jadi gejala adanya infeksi dari virus. Sebelum penyakit-penyakit ini mengganggu aktivitas kita, ada baiknya kita melakukan pencegahannya.</p>
                <p>Pencegahan paling awal adalah mengkonsumsi makanan sehat dan bergizi. Selain itu kita juga perlu melakukan olahraga cukup supaya badan tetap bugar. Terakhir adalah menambahkan minuman tradisional yang herbal  seperti yang terkandung dalam Larutan Penyegar Cap Badak.</p>
                <p>Larutan Penyegar Cap Badak merupakan minuman tradisional herbal yang aman untuk dijadikan pilihan ketika tenggorokan mulai terasa tidak nyaman. Tidak hanya ketika sakit, minuman sehat ini juga bisa kita nikmati saat keadaan sehat untuk menambah kebugaran tubuh. Minuman ini juga bisa kita simpan di kulkas dengan jangka waktu panjang. Larutan penyegar ini juga aman untuk dikonsumsi setiap hari baik oleh orang dewasa maupun anak anak selama masih dalam aturan konsumsi.</p>
                <p>Sudah saatnya kita sama-sama menjaga kesehatan di musim pancaroba seperti sekarang. Sediakan juga Larutan Penyegar Cap Badak di rumahmu untuk menjadi penolong pertama ketika tenggorokan terasa kurang sehat. </p>
                <p>Tunggu apa lagi? Langsung stok Larutan Penyegar Cap Badak sebagai teman sehatmu.</p>
                </div>
                
                <h3>Kata Mereka tentang <br>
                    Larutan Penyegar Cap Badak</h3>
            </div>


        </div>

        {{-- Lelang Carousel --}}
        <div class="lp_wgm_carousel">

            <div class="lp_wgm_carousel_item">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/landing-page/testi1.png') }}" alt="" class="img-fluid">
                    </div>
                    <div class="card-footer">
                        <h5>#1 Sariawan</h5>
                    </div>
                </div>
            </div>

            <div class="lp_wgm_carousel_item">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/landing-page/testi2.png') }}" alt="" class="img-fluid">

                    </div>
                    <div class="card-footer">
                        <h5>#2 Panas Dalam</h5>
                    </div>
                </div>
            </div>

            <div class="lp_wgm_carousel_item">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/landing-page/testi3.png') }}" alt="" class="img-fluid">

                    </div>
                    <div class="card-footer">
                        <h5>#3 Gejala Sakit</h5>
                    </div>
                </div>
            </div>

            <div class="lp_wgm_carousel_item">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/landing-page/testi4.png') }}" alt="" class="img-fluid">

                    </div>
                    <div class="card-footer">
                        <h5>#4 Sakit Tenggorokan</h5>
                    </div>
                </div>
            </div>

            <div class="lp_wgm_carousel_item">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/landing-page/testi5.png') }}" alt="" class="img-fluid">

                    </div>
                    <div class="card-footer">
                        <h5>#5 Masuk Angin</h5>
                    </div>
                </div>
            </div>

            <div class="lp_wgm_carousel_item">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/landing-page/testi6.png') }}" alt="" class="img-fluid">

                    </div>
                    <div class="card-footer">
                        <h5>#6 Pengganti
                            Air Mineral</h5>
                    </div>
                </div>
            </div>

            <div class="lp_wgm_carousel_item">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/landing-page/testi7.png') }}" alt="" class="img-fluid">

                    </div>
                    <div class="card-footer">
                        <h5>#7 Bibir Kering</h5>
                    </div>
                </div>
            </div>

            <div class="lp_wgm_carousel_item">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/landing-page/testi8.png') }}" alt="" class="img-fluid">

                    </div>
                    <div class="card-footer">
                        <h5>#8 Teman WFH</h5>
                    </div>
                </div>
            </div>

            <div class="lp_wgm_carousel_item">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/landing-page/testi9.png') }}" alt="" class="img-fluid">

                    </div>
                    <div class="card-footer">
                        <h5>#9 Habis Vaksin</h5>
                    </div>
                </div>
            </div>

            <div class="lp_wgm_carousel_item">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/landing-page/testi10.png') }}" alt="" class="img-fluid">

                    </div>
                    <div class="card-footer">
                        <h5>#10 Pembangkit Mood</h5>
                    </div>
                </div>
            </div>


        </div>


        <div class="container">
            
            <div class="lp_wgm_foot">
                <h3>Mereka aja udah buktiin, kamu kapan?</h3>

                <a href="https://sobatbadak.club/register" target="_blank">
                    <button>GABUNG KE CLUB SOBAT BADAK</button>
                </a>
            </div>
        </div>

    </section>
@endsection

@section('js')
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script>
        $('.lp_wgm_carousel').slick({
            centerMode: true,
            centerPadding: '60px',
            arrows: true,
            slidesToShow: 5,
            autoplay: true,
            autoplaySpeed: 2000,
            infinite: true,
            nextArrow: '.slick-next',
            prevArrow: '.slick-prev',
            responsive: [{
                    breakpoint: 1441,
                    settings: {
                        slidesToShow: 3
                    }
                }, {
                    breakpoint: 769,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                }
            ]
        });
    </script>

@endsection