@extends('layouts.app')

@section('content')
    <section class="redeem">
      <div class="redeem-header">
        <h1>Frequently Asked Questions</h1>
        <h5 style="opacity: 0.5">Reseller Koin Gatotkaca</h5>
      </div>
      
      <div class="container">
          <div class="faq_wrapper">
              
                <div id="accordionFAQ" class="faq__accordion accordion accordion-svg-toggle">

    <div class="card">
        <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
            aria-controls="collapseOne">
            <h5 class="mb-0">
                Apakah ada minimal order untuk join reseller?
            </h5>
            <span class="svg-icon">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                </svg>
            </span>
        </div>

        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionFAQ">
            <div class="card-body">
                Untuk join menjadi reseller kami, anda hanya membeli minimal 1 karton produk Larutan Penyegar Cap Badak
                edisi Gatotkaca.
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header collapsed" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
            aria-controls="collapseTwo">
            <h5 class="mb-0">

                Apa keuntungan jadi Reseller?

            </h5>
            <span class="svg-icon">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                </svg>
            </span>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionFAQ">
            <div class="card-body">
                Keuntungan anda menjadi reseller kami, anda akan mendapatkan koin gatotkaca yang dapat dikumpulkan untuk
                di tukarkan kepada kami menjadi baper point. Dimana baper point tersebut bisa di tukar dengan hadiah
                lelang. Disamping itu anda juga akan mendapatkan harga reseller khusus untuk pembelian produk larutan
                penyegar cap badak edisi koin gatotkaca.
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header collapsed" id="headingThree" data-toggle="collapse" data-target="#collapseThree"
            aria-expanded="false" aria-controls="collapseThree">
            <h5 class="mb-0">

                Berapa harga produknya?

            </h5>
            <span class="svg-icon">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                </svg>
            </span>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionFAQ">
            <div class="card-body">
                Silahkan hubungi admin kami melalui whatsapp untuk menanyakan harga produk.
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header collapsed" id="headingFour" data-toggle="collapse" data-target="#collapseFour"
            aria-expanded="false" aria-controls="collapseFour">
            <h5 class="mb-0">

                Apakah ada deposit?

            </h5>
            <span class="svg-icon">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                </svg>
            </span>
        </div>
        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionFAQ">
            <div class="card-body">
                Tidak ada deposit untuk menjadi reseller kami, Hanya order minimal 1 karton dan anda bisa langsung
                berjualan produk kami di toko anda.
            </div>
        </div>
    </div>


    <div class="card">
        <div class="card-header collapsed" id="headingFive" data-toggle="collapse" data-target="#collapseFive"
            aria-expanded="false" aria-controls="collapseFive">
            <h5 class="mb-0">

                Apakah Terdapat gratis ongkir untuk pengiriman LPCBnya ?

            </h5>
            <span class="svg-icon">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                </svg>
            </span>
        </div>
        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionFAQ">
            <div class="card-body">
                Kami memberikan gratis ongkir pengiriman reguler dengan minimum pembelian 1 karton isi 24pcs larutan
                penyegar cap badak.
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header collapsed" id="headingSix" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false"
            aria-controls="collapseSix">
            <h5 class="mb-0">

                Apakah bisa order mix varian rasa?

            </h5>
            <span class="svg-icon">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                </svg>
            </span>
        </div>
        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionFAQ">
            <div class="card-body">
                Pembelian larutan penyegar cap badak edisi koin gatotkaca hanya bisa dibeli minimal 1 karton 1 rasa.
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header collapsed" id="headingSeven" data-toggle="collapse" data-target="#collapseSeven"
            aria-expanded="false" aria-controls="collapseSeven">
            <h5 class="mb-0">

                Gimana caranya mau join reseller?

            </h5>
            <span class="svg-icon">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                </svg>
            </span>
        </div>
        <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionFAQ">
            <div class="card-body">
                Ketentuan untuk menjadi reseller minimum order 1 karton untuk produk larutan penyegar cap badak edisi
                koin Gatotkaca isi 24 pcs/kaleng larutan penyegar cap badak serta 4 koin Gatotkaca
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header collapsed" id="headingEight" data-toggle="collapse" data-target="#collapseEight"
            aria-expanded="false" aria-controls="collapseEight">
            <h5 class="mb-0">

                Ada varian rasa apa aja?

            </h5>
            <span class="svg-icon">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path d="M12 19L12 5" stroke="black" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                    <path d="M5 12L12 5L19 12" stroke="black" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" />
                </svg>
            </span>
        </div>
        <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionFAQ">
            <div class="card-body">
                Terdapat 3 varian rasa yaitu :
                <ul>
                    <li>Rasa Leci</li>
                    <li>Rasa Jambu</li>
                    <li>Rasa Jeruk</li>
                </ul>
            </div>
        </div>


    </div>
</div>
      
      
      
    </section>
@endsection


@section('js')
<script>
  
</script>
@endsection