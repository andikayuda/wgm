@extends('layouts.app')

@section('content')
    <section class="lp_wgm_hero">
        <img src="{{ asset('images/landing-page/lp-wgm-1.png') }}" alt="" style="width: 100%">
    </section>

    <section class="section-content" style="margin-top: 50px">
        <div class="container">
            <div class="lp_wgm_heading">
                <h2>Cara Tukar Koin Jadi Baper Poin</h2>
            </div>

            <div class="lp_wgm_content">
                <div class="lp_wgm_list">
                    <div class="lp_wgm_num">1.</div>
                    <div class="lp_wgm_list_desc">
                        <h4><span>Daftar</span> di Club Sobat Badak</h4>
                        <p>Daftarkan dirimu di Club Sobat Badak dengan register di website sobatbadak.club</p>

                        <div class="text-center">
                            <a href="https://sobatbadak.club/register" target="_blank">
                                <button>Daftar</button>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="lp_wgm_list">
                    <div class="lp_wgm_num">2.</div>
                    <div class="lp_wgm_list_desc">
                        <h4><span>Masuk</span> ke halaman Tukar Koin GK</h4>
                        <p>Di halaman utama, klik submenu “Redeem Kupon & Tukar Koin GK”</p>


                        <img src="{{ asset('images/landing-page/lp-wgm-img1.png') }}" alt="" class="img-fluid">

                    </div>
                </div>

                <div class="lp_wgm_list">
                    <div class="lp_wgm_num">3.</div>
                    <div class="lp_wgm_list_desc">
                        <h4><span>Masukkan</span> Jumlah Koin Gatotkaca yang ingin ditukar </h4>
                        <p>Masukkan sesuai dengan koin yang kamu miliki dan mau kamu tukarkan menjadi Baper Poin. Nilai tiap
                            koin berbeda-beda, jadi perhatiin baik baik ya!</p>


                        <img src="{{ asset('images/landing-page/lp-wgm-img2.png') }}" alt="" class="img-fluid">

                    </div>
                </div>

                <div class="lp_wgm_list">
                    <div class="lp_wgm_num">4.</div>
                    <div class="lp_wgm_list_desc">
                        <h4><span>Masukkan</span> Data diri </h4>
                        <p>Isi data dirimu yang lengkap ya agar memudahkan kita mengetahui paket yang kamu kirim nantinya.
                        </p>


                        <img src="{{ asset('images/landing-page/lp-wgm-img3.png') }}" alt="" class="img-fluid">

                    </div>
                </div>

                <div class="lp_wgm_list">
                    <div class="lp_wgm_num">5.</div>
                    <div class="lp_wgm_list_desc">
                        <h4><span>Kirim</span> Koinmu ke Alamat yang tertera</h4>
                        <p>Setelah kamu mengisi data diri, kirimkan koin yang kamu telah masukkan ke alamat yang tertera
                        </p>
                    </div>
                </div>

                <div class="lp_wgm_list">
                    <div class="lp_wgm_num">6.</div>
                    <div class="lp_wgm_list_desc">
                        <h4>Admin akan <span>Memverifikasi</span> Koinmu </h4>
                        <p>Jika Admin telah menerima paket berisi koin darimu, Admin akan mengecek apakah jumlah koin sesuai
                            dengan yang diinput olehmu. Setelah itu, Admin akan mengisikan Baper Poin ke akun Club Sobat
                            Badakmu
                        </p>
                    </div>
                </div>


            </div>

            <div class="lp_wgm_foot">
                <h3>Kamu sudah mengerti kan? <br>
                    Yuk tukar sekarang!</h3>

                <a href="https://sobatbadak.club" target="_blank">
                    <button>MASUK KE CLUB SOBAT BADAK</button>
                </a>
            </div>



        </div>


    </section>
@endsection
