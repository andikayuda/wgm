@extends('layouts.app')

@section('content')
    <section class="banner">
        <div class="container">
            <div class="row banner__row justify-content-center" style="align-items:center;background:transparent">
                <div class="leftside col-md-6 col-12">
                    <p class="white_bold_54">
                        CUAN Jutaan Rupiah cukup jadi Reseller!
                    </p>
                    <p class="white_500_18" style="padding-top: 1rem">
                        Gabung jadi reseller Larutan Penyegar Cap Badak edisi Gatotkaca sekarang, kamu bisa dapatkan
                        keuntungan
                        sebanyak-banyaknya dengan cara yang mudah.
                        <br>
                        <br>
                        Cari tahu selengkapnya disini yuk!
                    </p>
                    <a href="javascript:void(0)" class="btn white__btn btn__daftar" style="margin-top: 2rem">
                        <div class="d-flex" style="align-items: center">
                            DAFTAR <svg class="ml-2" width="16" height="16" viewBox="0 0 16 16" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M1 7C0.447715 7 0 7.44772 0 8C0 8.55228 0.447715 9 1 9V7ZM15 9C15.5523 9 16 8.55228 16 8C16 7.44772 15.5523 7 15 7V9ZM8.70711 0.292893C8.31658 -0.0976311 7.68342 -0.0976311 7.29289 0.292893C6.90237 0.683417 6.90237 1.31658 7.29289 1.70711L8.70711 0.292893ZM15 8L15.7071 8.70711C15.8946 8.51957 16 8.26522 16 8C16 7.73478 15.8946 7.48043 15.7071 7.29289L15 8ZM7.29289 14.2929C6.90237 14.6834 6.90237 15.3166 7.29289 15.7071C7.68342 16.0976 8.31658 16.0976 8.70711 15.7071L7.29289 14.2929ZM1 9H15V7H1V9ZM7.29289 1.70711L14.2929 8.70711L15.7071 7.29289L8.70711 0.292893L7.29289 1.70711ZM14.2929 7.29289L7.29289 14.2929L8.70711 15.7071L15.7071 8.70711L14.2929 7.29289Z"
                                    fill="#231F33" />
                            </svg>
                        </div>
                    </a>
                </div>
                <div class="rightside col-md-6 col-12">
                    <img src="{{ asset('images/landing-page/img_banner.png') }}" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="how text-center container" style="margin-top: 2rem">
        <div class="title__section">
            <p class="black_bold_36">Gimana sih caranya?</p>
            <p class="black_500_18">3 langkah mudah untuk bergabung di Larutan <br> Penyegar Cap Badak</p>
        </div>
    </section>

    <section class="slider_section">
        {{-- Lelang Arrow --}}
        <div class="text-center slick__btn">
            <button class="slick-prev lelang_carousel_arrow btn" aria-label="Previous" type="button" style="">
                <i class="fas fa-chevron-left"></i>
            </button>

            <button class="slick-next lelang_carousel_arrow btn" aria-label="Next" type="button" style="">
                <i class="fas fa-chevron-right"></i>
            </button>
        </div>
        <div class="how_carousel">
            <div class="carousel__item row">
                <div class="col-md-6 col-12" style="padding-top: 1rem; margin-top:1rem">
                    <div class="d-flex" style="align-items: center">
                        <img style="width: 64px;height:64px" src="{{ asset('images/landing-page/1.png') }}" alt="">
                        <p class="carousel_title">
                            Isi Data Diri
                        </p>
                    </div>
                    <div>
                        <p class="carousel_desc">
                            Daftarkan diri kamu dengan isi form di halaman ini.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-12 text-center">
                    <div class="img__carousel"><img src="{{ asset('images/landing-page/isi_data.png') }}" alt=""></div>
                </div>
            </div>
            <div class="carousel__item row">
                <div class="col-md-6 col-12" style="padding-top: 1rem; margin-top:1rem">
                    <div class="d-flex" style="align-items: center">
                        <img style="width: 64px;height:64px" src="{{ asset('images/landing-page/2.png') }}" alt="">
                        <p class="carousel_title">
                            Konfirmasi Pendaftaran
                        </p>
                    </div>
                    <div>
                        <p class="carousel_desc">
                            Kamu akan dihubungi sales representatif dari kami untuk konfirmasi data diri.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-12 text-center">
                    <div class="img__carousel"><img src="{{ asset('images/landing-page/selesai.png') }}" alt=""></div>
                </div>
            </div>
            <div class="carousel__item row">
                <div class="col-md-6 col-12" style="padding-top: 1rem; margin-top:1rem">
                    <div class="d-flex" style="align-items: center">
                        <img style="width: 64px;height:64px" src="{{ asset('images/landing-page/3.png') }}" alt="">
                        <p class="carousel_title">
                            Order barang untuk toko mu
                        </p>
                    </div>
                    <div>
                        <p class="carousel_desc">
                            Order barang dari Warisan Gajahmada pusat.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-12 text-center">
                    <div class="img__carousel"><img src="{{ asset('images/landing-page/troli.png') }}" alt=""></div>
                </div>
            </div>
            <div class="carousel__item row">
                <div class="col-md-6 col-12" style="padding-top: 1rem; margin-top:1rem">
                    <div class="d-flex" style="align-items: center">
                        <img style="width: 64px;height:64px" src="{{ asset('images/landing-page/1.png') }}" alt="">
                        <p class="carousel_title">
                            Isi Data Diri
                        </p>
                    </div>
                    <div>
                        <p class="carousel_desc">
                            Daftarkan diri kamu dengan isi form di halaman ini.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-12 text-center">
                    <div class="img__carousel"><img src="{{ asset('images/landing-page/isi_data.png') }}" alt=""></div>
                </div>
            </div>
            <div class="carousel__item row">
                <div class="col-md-6 col-12" style="padding-top: 1rem; margin-top:1rem">
                    <div class="d-flex" style="align-items: center">
                        <img style="width: 64px;height:64px" src="{{ asset('images/landing-page/2.png') }}" alt="">
                        <p class="carousel_title">
                            Konfirmasi Pendaftaran
                        </p>
                    </div>
                    <div>
                        <p class="carousel_desc">
                            Kamu akan dihubungi sales representatif dari kami untuk konfirmasi data diri.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-12 text-center">
                    <div class="img__carousel"><img src="{{ asset('images/landing-page/selesai.png') }}" alt=""></div>
                </div>
            </div>
            <div class="carousel__item row">
                <div class="col-md-6 col-12" style="padding-top: 1rem; margin-top:1rem">
                    <div class="d-flex" style="align-items: center">
                        <img style="width: 64px;height:64px" src="{{ asset('images/landing-page/3.png') }}" alt="">
                        <p class="carousel_title">
                            Order barang untuk toko mu
                        </p>
                    </div>
                    <div>
                        <p class="carousel_desc">
                            Order barang dari Warisan Gajahmada pusat.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-12 text-center">
                    <div class="img__carousel"><img src="{{ asset('images/landing-page/troli.png') }}" alt=""></div>
                </div>
            </div>
        </div>
    </section>

    <section class="benefit container">
        <div class="row">
            <div class="col-md-6 col-12 left__benefit">
                <p class="black_bold_36">
                    KEUNTUNGAN jadi Reseller Larutan Penyegar Cap Badak edisi Gatotkaca
                </p>
                <p class="black_500_18" style="margin-top: 1rem">
                    Siapa sih yang ga tau Koin Gatotkaca? Koin VIRAL yang diburu masyarakat Indonesia ini bisa datangkan
                    peluang keuntungan besar buat KAMU!sia.
                </p>
                <a href="javascript:void(0)" class="btn primary__btn btn__daftar" style="margin-top: 3rem">
                    <div class="d-flex" style="align-items: center">
                        DAFTAR
                        <svg class="ml-2" width="16" height="16" viewBox="0 0 16 16" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M1 7C0.447715 7 0 7.44772 0 8C0 8.55228 0.447715 9 1 9V7ZM15 9C15.5523 9 16 8.55228 16 8C16 7.44772 15.5523 7 15 7V9ZM8.70711 0.292893C8.31658 -0.0976311 7.68342 -0.0976311 7.29289 0.292893C6.90237 0.683417 6.90237 1.31658 7.29289 1.70711L8.70711 0.292893ZM15 8L15.7071 8.70711C15.8946 8.51957 16 8.26522 16 8C16 7.73478 15.8946 7.48043 15.7071 7.29289L15 8ZM7.29289 14.2929C6.90237 14.6834 6.90237 15.3166 7.29289 15.7071C7.68342 16.0976 8.31658 16.0976 8.70711 15.7071L7.29289 14.2929ZM1 9H15V7H1V9ZM7.29289 1.70711L14.2929 8.70711L15.7071 7.29289L8.70711 0.292893L7.29289 1.70711ZM14.2929 7.29289L7.29289 14.2929L8.70711 15.7071L15.7071 8.70711L14.2929 7.29289Z"
                                fill="white" />
                        </svg>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-12 right__benefit">
                <div class="row" style="margin-top: 2rem">
                    <div class="col-md-3 col-12 d-flex justify-content-center" style="align-items: center">
                        <img src="{{ asset('images/landing-page/ben_1.png') }}" alt="">
                    </div>
                    <div class="col-md-9 col-12">
                        <p class="c32_bold_24">
                            Koin VIRAL yang terjual 100 pack
                            setiap harinya!
                        </p>
                        <p class="black_500_18">
                            Ga perlu ragu, koin ini lagi diburu banyak orang!
                        </p>
                    </div>
                </div>
                <div class="row" style="margin-top: 2rem">
                    <div class="col-md-3 col-12 d-flex justify-content-center" style="align-items: center">
                        <img src="{{ asset('images/landing-page/ben_2.png') }}" alt="">
                    </div>
                    <div class="col-md-9 col-12">
                        <p class="c32_bold_24">
                            CUAN sampai dengan 25%!
                        </p>
                        <p class="black_500_18">
                            Produknya cepet ludes , dapet profit nya juga besar!
                        </p>
                    </div>
                </div>
                <div class="row" style="margin-top: 2rem">
                    <div class="col-md-3 col-12 d-flex justify-content-center" style="align-items: center">
                        <img src="{{ asset('images/landing-page/ben_3.png') }}" alt="">
                    </div>
                    <div class="col-md-9 col-12">
                        <p class="c32_bold_24">
                            Materi promosi DISEDIAIN!
                        </p>
                        <p class="black_500_18">
                            Terima beres, semua bahan konten design promosi akan dikirimkan. Tinggal simpan, copy lalu
                            posting di toko mu!
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="highlight container">
        <p class="black_bold_36 text-center">
            Perlu buktinya?
        </p>
        <p class="grey_500_18 text-center" style="margin-top: .5rem">
            Buat kalian yang masih ragu, coba lihat buktinya deh! <br> 
            Viral, mudah, dan bisa peluang keuntungan buat kamu!
        </p>
        <div class="d-flex" style="align-items: center;padding-top: 2rem">
            <img class="tabel__img" src="{{ asset('images/landing-page/tabel_toko.png') }}" alt="">
            <img class="sign_hand" src="{{ asset('images/landing-page/champ.png') }}" alt="">
        </div>
        <div class="d-flex" style="align-items: center">
            <img class="sign_hand" src="{{ asset('images/landing-page/like.png') }}" alt="">
            <img class="tabel__img" src="{{ asset('images/landing-page/tabel_produk.png') }}" alt="">
        </div>
    </section>

    <section class="gabung container" id="gabung_form">
        <div class="row">
            <div class="col-md-6 col-12 right__gabung rg_1">
                <img src="{{ asset('images/landing-page/phone.png') }}" alt="">
            </div>
            <div class="col-md-6 col-12 left__gabung">
                <p class="black_bold_36">
                    Ayo gabung jadi Reseller
                    Larutan Penyegar Cap Badak edisi Gatotkaca sekarang!
                </p>
                <p class="black_500_18" style="margin-top: .5rem">
                    Tunggu apalagi? Raih keuntunganmu dengan bergabung di Reseller Larutan Penyegar Cap Badak.
                </p>

                <form action="{{ route('postReseller') }}" method="post" class="form_daftar">
                    @csrf
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Nama" name="nama" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email" name="email" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control" placeholder="No. Whatsapp" name="whatsapp" autocomplete="off" required>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="linktoko">
                        <label class="form-check-label" for="linktoko">Saya sudah memiliki toko online</label>
                    </div>
                    <div class="form-group" id="url_tf">
                        <input type="url" class="form-control" placeholder="Link Toko" name="linkToko" autocomplete="off">
                        <span style="font-size: 12px; opacity: 0.4">*Harap isi dengan contoh format seperti ini: <em>https://namaweb.xyz</em></span>
                    </div>

                    <button type="submit" class="btn primary__btn" style="margin-top: 1rem">Daftar Sekarang</button>
                </form>
            </div>
            <div class="col-md-6 col-12 right__gabung rg_2">
                <img src="{{ asset('images/landing-page/phone.png') }}" alt="">
            </div>
        </div>
    </section>

    <section class="whatsapp container" style="margin-bottom: 5rem">
        <div class="row">
            <div class="col-md-4 col-12">
                <img src="{{ asset('images/landing-page/girl_phone.png') }}" alt="">
            </div>
            <div class="col-md-8 col-12">
                <p class="black_bold_36">
                    Mau lebih yakin? Sini tanya-tanya dulu
                    ke Admin kita, bebas tanya sepuasnya!
                </p>
                <a href="https://wa.me/{{$number->no_whatsapp}}?text=Halo%20min,%20Saya%20mau%20jadi%20Reseller%20Koin%20Gatotkaca"
                    target="_blank" class="btn wa__btn" style="margin-top: 3rem">
                    <div class="d-flex" style="align-items: center">
                        <img class="mr-2" src="https://warisangajahmada.com/images/wa.svg" alt="">
                        <div>Tanya via Whatsapp</div>
                    </div>
                </a>
            </div>
        </div>
    </section>
        
    <div class="modal fade" id="modal-sukses" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header" style="border:transparent">
            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body text-center">
            <img src="{{ asset('images/landing-page/check.png') }}" alt="" style="margin-bottom: 25px">
            <h5>Berhasil Daftar!</h5>
          </div>
          <div class="modal-footer" style="border:transparent">
            
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="modal-failed" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header" style="border:transparent">
            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body text-center">
            <img src="{{ asset('images/xicon.svg') }}" alt="" style="margin-bottom: 25px">
            <h5>Email sudah terdaftar!</h5>
          </div>
          <div class="modal-footer" style="border:transparent">
            
          </div>
        </div>
      </div>
    </div>
    
    
    
    
@endsection

@section('js')
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <!-- Include this after the sweet alert js file -->
    <script>
        $(function() {
            $('#url_tf').hide()
            $('#linktoko').change(function() { 
                if (this.checked) {
                    $('#url_tf').show()
                }else{
                    $('#url_tf').hide()
                }
            });
        })

        $('.how_carousel').slick({
            centerMode: true,
            arrows: true,
            slidesToShow: 3,
            contentPadding: '20px',
            // autoplay: true,
            autoplaySpeed: 2000,
            infinite: true,
            variableWidth: true,
            nextArrow: '.slick-next',
            prevArrow: '.slick-prev',
            responsive: [{
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                }
            ]
        });
    </script>
    
    @if (Session::has('success'))
        <script>
            $('#modal-sukses').modal('toggle');
        </script>
    @endif
        
    @if (Session::has('error'))
        <script>
            $('#modal-failed').modal('toggle');
        </script>
    @endif
    <script>
        $(".btn__daftar").click(function() {
            $('html,body').animate({
                scrollTop: $("#gabung_form").offset().top - 100},
                'slow');
        });
    </script>
    
@endsection