<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Leaderboard</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{ URL::to('css/leaderboard.css') }}">

</head>

<body>
    <img src="{{ asset('images/leaderboard/trophy-l.png') }}" alt="" class="img-fluid trophy-l">
    <img src="{{ asset('images/leaderboard/trophy-r.png') }}" alt="" class="img-fluid trophy-r">
    <div class="container">
        <div class="leaderboard">
            <h1>Leaderboard</h1>
            <a href={{ route('formLeaderboardPage') }}>
                <button class="join_event">Join Event</button>
            </a>
            <div class="leaderboard_podium">
            @if($leaderboard[1])
                <div class="winner winner-2">
                    <div>
                        <img src="{{ asset('images/leaderboard/winner2.svg') }}" alt="">
                    </div>
                    <div class="winner_thumb">
                        <img src="{{$leaderboard[1]->photo}}" alt="">
                    </div>
                    <h5 class="winner_name">{{$leaderboard[1]->tiktok_username}}</h5>
                    <h4 class="winner_likes">{{$leaderboard[1]->totalLike}}</h4>
                    <h5 class="likes">Likes</h5>
                </div>
            @endif
                @if($leaderboard[0])
                <div class="winner winner-1">
                    <div>
                        <img src="{{ asset('images/leaderboard/winner1.svg') }}" alt="">
                    </div>
                    <div class="winner_thumb">
                        <img src="{{$leaderboard[0]->photo}}" alt="">
                    </div>
                    <h5 class="winner_name">{{$leaderboard[0]->tiktok_username}}</h5>
                    <h4 class="winner_likes">{{$leaderboard[0]->totalLike}}</h4>
                    <h5 class="likes">Likes</h5>


                </div>
                @endif
                @if($leaderboard[2])
                <div class="winner winner-3">
                    <div>
                        <img src="{{ asset('images/leaderboard/winner3.svg') }}" alt="">
                    </div>
                    <div class="winner_thumb">
                        <img src="{{$leaderboard[2]->photo}}" alt="">
                    </div>
                    <h5 class="winner_name">{{$leaderboard[2]->tiktok_username}}</h5>
                    <h4 class="winner_likes">{{$leaderboard[2]->totalLike}}</h4>
                    <h5 class="likes">Likes</h5>


                </div>
                @endif
            </div>
            <div class="leaderboard_table desktop">
                <table>
                    <thead>
                        <tr>
                            <th>Rank</th>
                            <th>Account</th>
                            <th>#Main_Hashtag</th>
                            <th>Likes</th>
                            <th>Shares</th>
                            <th>Comment</th>
                            <th>Views</th>
                            <th>Category</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php
                    $i = 1;
                    @endphp
                    @foreach($datas as $data)
                        <tr>
                            <td>{{$i}}</td>
                            <td><img src="{{$data->photo}}" alt="avatar" width="30"
                                    class="mr-2">
                                {{$data->tiktok_username}}</td>
                            <td>#{{$data->mainHashtag}}
                                    @if($data->hashtagUsage <= 1000 && $data->hashtagUsage>=100)
                                    <img class="ml-2" src={{ asset('images/leaderboard/bronze.svg') }}
                                    alt="bronze" data-toggle="tooltip" data-placement="bottom" title="Popularity Point <br>{{$data->hashtagUsage}}" data-html="true">
                                    @elseif($data->hashtagUsage >= 1001 && $data->hashtagUsage <= 5000)
                                    <img class="ml-2" src={{ asset('images/leaderboard/silver.svg') }}
                                    alt="silver" data-toggle="tooltip" data-placement="bottom" title="Popularity Point <br>{{$data->hashtagUsage}}" data-html="true">
                                    @elseif($data->hashtagUsage >= 5001)
                                        <img class="ml-2" src="{{ asset('images/leaderboard/gold.svg') }}" alt="gold"
                                        data-toggle="tooltip" data-placement="bottom" title="Popularity Point<br>{{$data->hashtagUsage}}" data-html="true">
                                    @endif
                                   
                            </td>
                            <td>{{$data->totalLike}}</td>
                            <td>{{$data->totalShare}}</td>
                            <td>{{$data->totalComment}}</td>
                            <td>{{$data->totalPlay}}</td>
                            @if($data->hashtagId == NULL)
                            <td>-</td>
                            @else
                            <td>#{{$data->hashtag}}</td>
                            @endif
                        </tr>
                    @php $i++ @endphp
                    @endforeach
                    </tbody>
                </table>
            </div>

            {{-- =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= --}}
            <div class="leaderboard_table mobile">
                <table>
                    <thead>
                        <tr>
                            <th>Rank</th>
                            <th>Account</th>
                            <th>Likes</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php
                    $i = 1;
                    @endphp
                        @foreach($datas as $data)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$data->tiktok_username}}</td>
                            <td>{{$data->totalLike}}</td>
                        </tr>
                        @php $i++ @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="syarat-ketentuan">
                <h3>Syarat mengikuti Kompetisi <br>
                    Sinde Tiktok Challenge</h3>
                <div class="row">
                    <div class="col-md-4">
                        {{-- <img src="/img/syarat-1.png" alt=""> --}}
                        <img src="{{ asset('images/leaderboard/syarat-1.png') }}" alt="">
                        <p>Min. 5000 views</p>
                    </div>
                    <div class="col-md-4">
                        {{-- <img src="/img/syarat-2.png" alt=""> --}}
                        <img src="{{ asset('images/leaderboard/syarat-2.png') }}" alt="">
                        <p>Approved by Admin</p>
                    </div>
                    <div class="col-md-4">
                        {{-- <img src="/img/syarat-3.png" alt=""> --}}
                        <img src="{{ asset('images/leaderboard/syarat-3.png') }}" alt="">
                        <p>Win by creativity</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>

    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })

    </script>
</body>

</html>
