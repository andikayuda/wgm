<div class="card mb-20">
  <div class="card-header text-center">
    <h4>Hasil Undian {{$hadiah}}</h4>
  </div>
  <div class="card-body">
    <h1 class="big">{{$kode}}</h1>
  </div>
  <div class="card-footer">
      @if ($kode != "*****")
        @foreach ($list as $x => $p)
          <p>{{$p->kodeUndian}} | {{$p->nama}} | {{$p->kode_form}}</p>

        @endforeach
      @endif
  </div>
</div>
