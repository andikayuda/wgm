@extends('layouts.app')

@section('cssPage')
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="{{ URL::asset('css/event-marketplace.css') }}">

@endsection

@section('content')
    <div class="d-flex flex-column-fluid content">

        <!--begin::Container-->

        <div class="container-fluid banner">
            <section class="banner-hero">
                <div class="swiper mySwiper">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">

                            <a href="">
                                <img src="https://images.tokopedia.net/img/cache/1200/BgtCLw/2021/10/14/43616251-389f-4c16-9bc7-52b93182e331.jpg"
                                    alt="">
                            </a>


                        </div>
                        <div class="swiper-slide">

                            <a href="">
                                <img src="https://images.tokopedia.net/img/cache/1200/BgtCLw/2021/8/4/7d57f774-09b4-4eb2-9041-c277fda10ba8.jpg"
                                    alt="">
                            </a>
                        </div>

                        <div class="swiper-slide">

                            <a href="">
                                <img src="https://images.tokopedia.net/img/cache/1200/BgtCLw/2021/10/14/43616251-389f-4c16-9bc7-52b93182e331.jpg"
                                    alt="">
                            </a>

                        </div>

                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-pagination"></div>
                </div>
            </section>
            <div class="divider"></div>
            <section class="section-alasan ">
                <div class="container">

                    <h1 class="mt-5 mb-8 title-part">Kenapa sih kamu harus pakai <a href="" class="link-string">Warisan
                            Gajahmada</a>?</h1>
                    <div class="row ">

                        <div class="col-lg-4">
                            <div class="card card-alasan red mb-7">
                                <div class="card-body ">
                                    <h3 class="title-alasan text-center">Produk Pasti Original</h3>
                                    <p class="detail-alasan ">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-1">

                        </div>
                        <div class="col-lg-4">
                            <div class="card card-alasan red mb-7" >
                                <div class="card-body ">
                                    <h3 class="title-alasan text-center">Orderan Cepat</h3>
                                    <p class="detail-alasan ">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3"></div>
                    </div>
                    <div class="row mb-5">
                        <div class="col-lg-3">

                        </div>
                        <div class="col-lg-4">
                            <div class="card card-alasan red mb-7">
                                <div class="card-body ">
                                    <h3 class="title-alasan text-center">Chat Responsive</h3>
                                    <p class="detail-alasan ">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-1"></div>
                        <div class="col-lg-4">
                            <div class="card card-alasan red mb-7">
                                <div class="card-body ">
                                    <h3 class="title-alasan text-center">Packing Rapih dan Aman</h3>
                                    <p class="detail-alasan ">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua.
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <div class="divider"></div>
            <section class="section-cara-membeli">
                <div class="container ">
                    <h1 class="title-part">Bagaimana cara beli produk Warisan Gajahmada?</h1>
                    <div class="row mt-5">
                        <div class="col-md-6">
                            <div class="card membeli mb-7">
                                <div class="card-body">
                                    <h5 class="title-part">
                                        Tidak tau cara untuk beli produk di platform tersebut? Nih ada penjelasannya
                                    </h5>
                                    <div class="row mt-4">
                                        <div class="col-md-4">
                                            <h6 class="title-part red" style="text-align: start">Tokopedia</h6>
                                        </div>
                                        <div class="col-md-8">
                                            <p class="detail-data">Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit, sed do eiusmod tempor
                                                incididunt ut labore et dolore magna aliqua.</p>
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-md-4">
                                            <h6 class="title-part red" style="text-align: start">Shopee</h6>
                                        </div>
                                        <div class="col-md-8">
                                            <p class="detail-data">Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit, sed do eiusmod tempor
                                                incididunt ut labore et dolore magna aliqua.</p>
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-md-4">
                                            <h6 class="title-part red" style="text-align: start">Blibli</h6>
                                        </div>
                                        <div class="col-md-8">
                                            <p class="detail-data">Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit, sed do eiusmod tempor
                                                incididunt ut labore et dolore magna aliqua.</p>
                                        </div>
                                    </div>
                                    <h3 class="title-part mt-5"> Masih kurang jelas? Tanya admin kita di admin@warisan.com</h3>
                                </div>
                            </div>


                        </div>
                        <div class="col-md-6">
                            <div class="card ">
                                <div class="card-body">
                                    <h4 class="title-part red mb-7">Klik di tombol di bawah ini buat cek toko kami!</h4>
                                    <div class="d-flex justify-content-center">
                                        <div class="toko-online ">
                                            <a href="" class="btn tokped">
                                                <img src="https://warisangajahmada.com/./images/tokped.svg" alt="">
                                                Tokopedia
                                            </a>
                                            <a href="" class="btn shopee">
                                                <img src="https://warisangajahmada.com/./images/sopi.svg" alt="">
                                                Shopee
                                            </a>
                                            <a href="" class="btn blibli">
                                                <img src="https://warisangajahmada.com/./images/blibli.svg" alt="">
                                                Blibli
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </section>
            <div class="divider"></div>
            <section class="section-comment">
                <div class="container">
                    <h1 class="title-part mb-5">Kesaksian Mereka Yang Sudah Beli</h1>
                    @for ($i = 0; $i < 6; $i++)
                    @component('components.custom.comment')
                        @slot('name')
                            Lorem Ipsum.
                        @endslot
                        @slot('detail')
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua.
                        @endslot
                        @slot('date')
                            11 Desember 2021
                        @endslot
                    @endcomponent
                @endfor
                </div>
                
            </section>
            <div class="divider"></div>
        </div>

        <!--end::Container-->
    </div>
@endsection


@section('js')
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script>
        var swiper = new Swiper(".mySwiper", {
            slidesPerView: 1,
            spaceBetween: 30,
            loop: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });
    </script>
@endsection
<!--begin::Entry-->


<!--end::Entry-->
