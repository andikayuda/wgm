@extends('layouts.app')

@section('content')
    <section class="container-fluid ads-header-wrapper">
      <div class="ads-header">
        {{-- <div class="ads-header-img"> --}}
        @if ($device)
          <img src="{{asset($data->banner_mobile)}}" alt="" class="ads-header-img-mobile">
        @else    
          <img src="{{asset($data->brandImage)}}" alt="" class="ads-header-img">
        @endif  
        {{-- </div> --}}
          @if ($data->brand == 'IPhone 12 Pro' || $data->brand == 'Tokopedia')
            <div class="ads-text-header">
              <h3 class="black">Nih ada warisan buat kamu,</h3>
              <h1 class="black">
              {{$data->title}}
              </h1>
              <h6 class="black">
              {{$data->subTitle}}
              </h6>
            </div>
          @elseif ($data->brand == 'ps5')
            <div class="ads-text-header">
              <h3 class="blue">Nih ada warisan buat kamu,</h3>
              <h1 class="blue">
              {{$data->title}}
              </h1>
              <h6 class="blue">
              {{$data->subTitle}}
              </h6>
            </div>
            @elseif ($data->brand == 'pulsa 200k')
              <div class="ads-text-header">
                <h3 class="purple">Nih ada warisan buat kamu,</h3>
                <h1 class="purple">
                {{$data->title}}
                </h1>
                <h6 class="purple">
                {{$data->subTitle}}
                </h6>
              </div>
            @elseif ($data->brand == 'Sepeda' || $data->brand == 'Diamond FF' || $data->brand == 'Diamond ML')
              <div class="ads-text-header">
                <h3 class="white">Nih ada warisan buat kamu,</h3>
                <h1 class="white">
                {{$data->title}}
                </h1>
                <h6 class="white">
                {{$data->subTitle}}
                </h6>
              </div>
            {{-- @elseif ($data->brand == 'Tokopedia')
              <div class="ads-text-header">
                <h3 class="purple">Nih ada warisan buat kamu,</h3>
                <h1 class="purple">
                {{$data->title}}
                </h1>
                <h6 class="purple">
                {{$data->subTitle}}
                </h6>
              </div>
            @elseif ($data->brand == 'Diamond ML')
              <div class="ads-text-header">
                <h3 class="purple">Nih ada warisan buat kamu,</h3>
                <h1 class="purple">
                {{$data->title}}
                </h1>
                <h6 class="purple">
                {{$data->subTitle}}
                </h6>
              </div>
            @elseif ($data->brand == 'Diamond FF')
              <div class="ads-text-header">
                <h3 class="purple">Nih ada warisan buat kamu,</h3>
                <h1 class="purple">
                {{$data->title}}
                </h1>
                <h6 class="purple">
                {{$data->subTitle}}
                </h6>
              </div> --}}
            @endif
      </div>
        <div class="content">
          <span>Cara</span>
          <span>Mendapatkan</span>
          <div class="step1">
            <div class="row justify-content-center">
              <div class="col-md-3 prize-image-wrapper">
                <img src="{{URL::asset('images/prize_step_1.png')}}" alt="" class="prize-step-image1">
              </div>
              {{-- <div class="col-1 prize-step-numbers">
                <h1>1.</h1>
              </div> --}}
              <div class="col-md-7 prize-step-1-wrapper" >
                <div class="row prize-step-1">
                  <div class="col-1 prize-step-numbers">
                    <h1>1.</h1>
                  </div>
                  <div class="col-10 prize_step_text">
                    <span>Beli</span>
                    <span>Larutan Penyegar</span>
                    <br>
                    <span>Cap Badak</span>
                    <p>Beli paket Bundle isi 6 pcs kaleng Larutan Penyegar Cap Badak yang sudah berisi Koin Gatotkaca.</p>
                    <a class="btn btn-danger" href="{{route('home').'/#shopping-list'}}" role="button">PESAN SEKARANG</a> 
                  </div>
                </div>                       
              </div>
            </div>
          </div>
          <div class="container-fluid step2">
            <div class="row prize-step-2 justify-content-center">
              <div class="col-1">
                <h1 class="number-step-1">2.</h1>
              </div>
              <div class="col-10 prize_step_text-2">
                <span>Kumpulkan</span>
                <span>Koin Gatotkaca</span>
                <p>{{$data->coinText}}</p>
              </div>
              
              @if ($data->slug == "ps5" || $data->slug == "iphone-12-pro" || $data->slug == "sepeda" || $data->slug == "pulsa-200k")
                <div class="img-ads-wrapper">
                  @if($device)
                    <img src="{{asset($data->mobileCoin)}}" alt="" class="img-ads-mobile">
                  @else   
                    <img src="{{asset($data->coinBrand)}}" alt="" class="img-ads-web">
                  @endif
                </div>
              @else 
                @if($device)
                  <h1>Koin yang harus ditukarkan</h1>
                  <div class="swiper-mobile-container">
                    <div class="swiper-wrapper">
                      @foreach ($pictures as $item)
                        <div class="swiper-slide">
                          <h1>{{$item->prize}}</h1>
                          <div>
                            <img src="{{asset($item->imageMob)}}" alt="">
                          </div>
                        </div>
                      @endforeach
                    </div>
                  </div>
                @else 
                  <table class="table-koin-redeem-ads">
                    <thead>
                      <tr>
                        <th scope="col">Jumlah Diamond</th>
                        <th scope="col">Koin yang dibutuhkan</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($pictures as $item)
                        <tr>
                          <td>{{$item->prize}}</td>
                          <td><img src="{{asset($item->imageWeb)}}" alt="" class="ads-img-table"></td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                @endif
              @endif
              
            </div>
          </div>
          <div class="step3">
            <div class="row justify-content-center">
              <div class="col-md-7 step3-text">
                <div class="row prize-step-3">
                  <div class="col-1 prize-step-numbers-3">
                    <h1>3.</h1>
                  </div>
                  <div class="col-10 prize_step_text-3">
                    <span>Tukarkan</span>
                    <span>Koin Gatotkaca</span>
                    <p>Setelah kamu mengumpulkan semua koinnya, tukarkan koinmu disini</p>
                    <a class="btn btn-danger" href="{{route('redeem')}}" role="button">TUKAR SEKARANG</a>            
                  </div>
                </div>
              </div>
              <div class="col-md-3 img-step-3-wrapper">
                <img src="{{URL::asset('images/prize_step_1.png')}}" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
        <div class="ads-penawaran-wrapper">
          <h1>Lihat Penawaran Lainnya</h1>
          <div class="swiper-container">
            <div class="swiper-wrapper">
              {{-- @foreach ($carousel as $item)
                <div class="swiper-slide">
                <a href="">
                  <div class="ads-img-slide">
                    <img src="{{URL::asset('images/iphone.png')}}" alt="">
                  </div>
                  <h3>Iphone</h3>
                </a>
              </div>
              @endforeach --}}
              <div class="swiper-slide">
                <a href="{{ route('ads', ['slug' => 'iphone-12-pro']) }}">
                  <div class="ads-img-slide">
                    <img src="{{URL::asset('images/iphone.png')}}" alt="">
                  </div>
                  <h3>Iphone</h3>
                </a>
              </div>
              <div class="swiper-slide">
                <a href="{{ route('ads', ['slug' => 'ps5']) }}">
                  <div class="ads-img-slide">
                    <img src="{{URL::asset('images/ps5.png')}}" alt="">
                  </div>
                  <h3>Ps5</h3>
                </a>
              </div>
              <div class="swiper-slide">
                <a href="{{ route('ads', ['slug' => 'sepeda']) }}">
                  <div class="ads-img-slide">
                    <img src="{{URL::asset('images/bike.png')}}" alt="">
                  </div>
                  <h3>Sepeda</h3>
                </a>
              </div>
              <div class="swiper-slide">
                <a href="{{ route('ads', ['slug' => 'pulsa-200k']) }}">
                  <div class="ads-img-slide">
                    <img src="{{URL::asset('images/cell_operator.png')}}" alt="">
                  </div>
                  <h3>Pulsa 200k</h3>
                </a>
              </div>
              <div class="swiper-slide">
                <a href="{{ route('ads', ['slug' => 'diamond-ff']) }}">
                  <div class="ads-img-slide">
                    <img src="{{URL::asset('images/FF.png')}}" alt="">
                  </div>
                  <h3>Diamond FF</h3>
                </a>
              </div>
              <div class="swiper-slide">
                <a href="{{ route('ads', ['slug' => 'diamond-ml']) }}">
                  <div class="ads-img-slide">
                    <img src="{{URL::asset('images/ML.png')}}" alt="">
                  </div>
                  <h3>Diamond ML</h3>
                </a>
              </div>
              <div class="swiper-slide">
                <a href="{{ route('ads', ['slug' => 'tokopedia']) }}">
                  <div class="ads-img-slide">
                    <img src="{{URL::asset('images/TOKPED.png')}}" alt="">
                  </div>
                  <h3>Voucher Belanja</h3>
                </a>
              </div>
            </div>
          </div>
            <div class="swiper-pagination swiper-pagination-black"></div>
        </div>
    </section>
@endsection

@section('js')
<script src="{{ URL::asset('js/swiper-bundle.min.js') }}"></script>

<script>
  var swiper = new Swiper('.swiper-container', {
    slidesPerView: 3,
    spaceBetween: 30,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    breakpoints: {
      320 : {
        slidesPerView: 3,
        spaceBetween: 5,
      },
      525 : {
        slidesPerView: 3,
        spaceBetween: 20,
      },
    }
    // swiper.on()
  });
</script>
<script>
  var swiper = new Swiper('.swiper-mobile-container', {
    slidesPerView: 1,
    // spaceBetween: 30,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    // swiper.on()
  });
</script>
@endsection