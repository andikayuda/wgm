@extends('layouts.app')

@section('content')
    <section class="redeem">
      <div class="redeem-header">
        <h1>Penukaran Koin Gatotkaca</h1>
      </div>
      
      <!--<div class="container redeem-banner">-->
      <!--    <img class="redeem-desktop" src="{{ asset('./images/sorry.png') }}" alt="sorry">-->
      <!--    <img class="redeem-mobile" src="{{ asset('./images/sorry-mobile.png') }}" alt="sorry">-->
      <!--</div>-->
      
      <div class="container redeem-content">
        <span>Isi Data</span>
        <span>Berikut</span>
        <form action="{{ route('redeemForm') }}" method="post">
          @csrf
          <div class="form-group redeem-category">
            <select name="pilihan" class="form-control redeem-select" id="select-redeem" >
              <option disabled selected>----Pilih Hadiah Redeem----</option>
              @foreach ($dataRedeem as $datum)
              @if($datum->status == 1 && $datum->kategori == 'Single')
                <option value='{{$datum->id}}'>{{$datum->brand}} - Kuota Hadiah Tersisa: {{$datum->kuotaPenukaran}}</option>
              @elseif($datum->status == 1 && $datum->kategori == 'Game')
                <option value='{{$datum->id}}'>{{$datum->brand}} - Kuota Hadiah Tersisa: Klik Untuk Lanjut</option>
              @endif
              @endforeach
            </select>
          </div>
          <div class="form-group redeem-game redeem-form-hide" id="redeem-game-bottom">
            <select name="tipeHadiah" class="form-control redeem-select" id="select-redeem-game" >

            </select>
          </div>
          <div class="koin-requirement">
            <p id=koin-kuantitas>Koin yang dibutuhkan</p>
            <img src="" id="img_koin_redeem" alt="">
          </div>
          <div class="form-group">
            <input type="text" class="form-control redeem-input" id="namaLengkap" name="nama" placeholder="Nama Lengkap" required >
          </div>
          <div class="form-group">
            <input type="text" class="form-control redeem-input" id="nomorWa" name="hp" placeholder="Nomor Whatsapp" required >
          </div>
          <div class="form-group">
            <input type="email" class="form-control redeem-input" id="alamatEmail" name="email" placeholder="Alamat e-mail" required >
          </div>
          <div class="form-group">
            <textarea class="form-control redeem-input" id="alamat-lengkap" rows="6" name="alamat" placeholder="Alamat Lengkap" required ></textarea>
          </div>
          <button class="btn btn-redeem-form" type="submit" disabled>SUBMIT</button>

        </form>
        @if(\Session::has('kodeRedeem'))
        <div class="alamat-redeem" id="kodeRedeemShow">
          <h3>Silahkan mengirim koin tersebut ke alamat di bawah ini</h3>
          <h3>Warisan Gajahmada</h3>
          <h3>Ruko Crystal 8 No. 18 Alam Sutera,></h3>
          <h3>Pakualam, Kec. Serpong Utara,</h3>
          <h3>Kota Tangerang Selatan, Banten 15320</h3>
        </div>
        
        <div class="kode-redeem-wrapper">
          <h3>Kode Redeem Kamu</h3>
          <input type="text" readonly class="form-control-plaintext redeem-code"  value="{{\Session::get('kodeRedeem')}}">
        </div>
        @endif
      </div>

      <div class="container-fluid lacak-status-wrapper">
        <form method="post" action="{{ route('redeemTracking') }}">
          @csrf
          <div class="form-group lacak-form">
            <label for="lacakStatus" class="lacak-label">Lacak status redeem anda di sini</label>
            <input type="text" class="form-control input-lacak" id="lacakStatus" name="kode">
            <button class="btn btn-lacak" type="submit">LACAK</button>
          </div>
        </form>
      </div>

        
      @if(!empty($data))
      
        <div class="progress-bar" id="redeemProgress">

            <h4>Status</h4>
            <h5>Kode Redeem: {{$data->kodeRedeem}}</h5>

          <ol class="progress-meter">
            <!--<li class="progress-point <?php echo ($data->statusRedeem === 'menunggu penerimaan koin') ? 'todo' : 'done'; ?> ">-->
            <li class="progress-point done">
              <p class="status-category">Permintaan Redeem dicatat<p> 
              <p class="status-detail">Permintaan Redeem dicatat</p>
            </li>
            <li class="progress-point <?php echo ($data->statusKoin === 'belum diterima') ? 'todo' : 'done'; ?>">
              <p class="status-category">Verifikasi Koin<p> 
              <p class="status-detail">koin {{$data->statusKoin}}</p>
            </li>
            <li class="progress-point <?php echo ($data->statusHadiah === 'sedang diproses' || $data->statusHadiah === 'belum diberikan') ? 'todo' : 'done'; ?>">
              <p class="status-category">Verifikasi hadiah<p> 
              <p class="status-detail">hadiah {{$data->statusHadiah}}</p>
            </li>
          </ol>
        </div>
      @endif
      
      {{-- <div class="progress-bar">
        <h3>status</h3>
        <div class="container">
          <ol class="progress-meter">
          <li class="progress-point done">
            <p class="status-category">Verifikasi Redeem<p> 
            <p class="status-detail">verfikasi redeem</p>
          </li>
          <li class="progress-point done">
            <p class="status-category">Verifikasi Redeem<p> 
            <p class="status-detail">verfikasi redeem</p>
          </li>
          <li class="progress-point todo">
            <p class="status-category">Verifikasi Redeem<p> 
            <p class="status-detail">verfikasi redeem</p>
          </li>
        </ol>
        </div>
      </div> --}}
      
      
    </section>
@endsection


@section('js')
<script>
  $(".redeem-select").on('click', function() {
        $('.redeem-select').css('color', 'black')
    })
//   var selectOptions = {
//       'ff': ['Diamond FF 1jt', 'Diamond FF 500rb', 'Diamond FF 300rb', 'Diamond FF 200rb', 'Diamond FF 100rb', 'Diamond FF 50rb', 'Diamond FF 20rb'],
//       'mole' : ['Diamond ML 1jt', 'Diamond ML 500rb', 'Diamond ML 300rb', 'Diamond ML 165rb', 'Diamond ML 120rb', 'Diamond ML 88rb', 'Diamond ML 71,5rb', 'Diamond ML 49rb'],
//       'tokped' : ['Voucher Belanja 1jt', 'Voucher Belanja 500rb', 'Voucher Belanja 250rb', 'Voucher Belanja 150rb'],
//   };

  $('#select-redeem').on('change', function() {
    var getText = $('#select-redeem option:selected').text();
    var qty = getText.slice(-1);
    console.log(qty, 'atas');
    if(qty == 0){
        $('#namaLengkap').attr('disabled', true);
        $('#nomorWa').attr('disabled', true);
        $('#alamatEmail').attr('disabled', true);
        $('#alamat-lengkap').attr('disabled', true);
        $('.btn-redeem-form').attr('disabled', true);
    }else{
        $('#namaLengkap').attr('disabled', false);
        $('#nomorWa').attr('disabled', false);
        $('#alamatEmail').attr('disabled', false);
        $('#alamat-lengkap').attr('disabled', false);
        $('.btn-redeem-form').attr('disabled', false);
        
    }
    var widthWindows = $(window).width();
      var selectValue = $(this).val();
    // console.log(selectValue)
      // console.log('masuk')
        // var subjectId = $('select[name=subject_id] option').filter(':selected').val();
        // console.log(subjectId);
        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: '/game/prize/' + selectValue,
            success: function(data) {
            //   console.log(data.success.brand);
                var datas = data.success;
                if(datas.length == undefined){
                  // console.log('masuk');
                    // $("#topicData").html(''); //clears container for new data
                    $('#img_koin_redeem').css('display', 'block');
                    if (widthWindows > 425){
                      var source = "https://warisangajahmada.com/"+datas.coinBrand
                    }else if (widthWindows <= 425) {
                      var source = "https://warisangajahmada.com/"+datas.mobileCoin
                    }
                    if(data.success.brand != "Uang Tunai 2 Juta"){
                    $("#img_koin_redeem").addClass('resize-koin');
                        console.log('benar')
                    }else{
                    $("#img_koin_redeem").removeClass('resize-koin');
                        console.log('benar')
                        
                    }
                    // $("#img_koin_redeem").addClass('resize-koin');
                    $('#img_koin_redeem').attr('src', source);
                    $("#redeem-game-bottom").addClass('redeem-form-hide');
                }
                else{
                //   console.log(datas)
                  $("#select-redeem-game").html(''); //clears container for new data
                  if (widthWindows > 425){
                      var source = "https://warisangajahmada.com/" + datas[0].imageWeb
                    }else if (widthWindows <= 425) {
                      var source = "https://warisangajahmada.com/" + datas[0].imageMob
                    }
                    $("#img_koin_redeem").css('display', 'block');
                  $("#img_koin_redeem").attr('src', source); //clears container for new data
                  $("#img_koin_redeem").removeClass('resize-koin');
                    
                $.each(datas, function(index, item) {
                                    // console.log('masuk');
                        // console.log(item.id);
                        $("#select-redeem-game").append(`
                            <option value="`+item.id+`">`+item.prize+` - Kuota Hadiah Tersisa: `+item.kuotaPenukaran+`</option>`);
                        
                });
                $("#redeem-game-bottom").removeClass('redeem-form-hide');
                var getText = $('#select-redeem-game option:selected').text();
                var qty = getText.slice(-2);
                console.log(qty);
                if(qty == 0){
                    $('#namaLengkap').attr('disabled', true);
                    $('#nomorWa').attr('disabled', true);
                    $('#alamatEmail').attr('disabled', true);
                    $('#alamat-lengkap').attr('disabled', true);
                    $('.btn-redeem-form').attr('disabled', true);
                }else{
                    $('#namaLengkap').attr('disabled', false);
                    $('#nomorWa').attr('disabled', false);
                    $('#alamatEmail').attr('disabled', false);
                    $('#alamat-lengkap').attr('disabled', false);
                    $('.btn-redeem-form').attr('disabled', false);
                    
                }
            }
                // $("#data").append(data);
            },
            error: function(err) {
                console.log(err);
            }
        });
    
  })


  $('#select-redeem-game').on('change', function() {
    var getText = $('#select-redeem-game option:selected').text();
        var qty = getText.slice(-2);
        console.log(qty, 'tes');
        if(qty == 0){
            $('#namaLengkap').attr('disabled', true);
            $('#nomorWa').attr('disabled', true);
            $('#alamatEmail').attr('disabled', true);
            $('#alamat-lengkap').attr('disabled', true);
            $('.btn-redeem-form').attr('disabled', true);
        }else{
            $('#namaLengkap').attr('disabled', false);
            $('#nomorWa').attr('disabled', false);
            $('#alamatEmail').attr('disabled', false);
            $('#alamat-lengkap').attr('disabled', false);
            $('.btn-redeem-form').attr('disabled', false);
            
        }
var widthWindows = $(window).width();
  var selectValue = $(this).val();

  // console.log(selectValue)
    // var subjectId = $('select[name=subject_id] option').filter(':selected').val();
    // console.log(subjectId);
    $.ajax({
        type: 'GET', //THIS NEEDS TO BE GET
        url: '/game/prize2/' + selectValue,
        success: function(data) {
          // console.log(data);
            var datas = data.success;
            // console.log(data)
              // console.log('masuk');
                $('#img_koin_redeem').css('display', 'block');
                if (widthWindows > 425){
                  var source = "https://warisangajahmada.com/"+ datas.imageWeb
                }else if (widthWindows <= 425) {
                  var source = "https://warisangajahmada.com/"+ datas.imageMob
                }
                $('#img_koin_redeem').attr('src', source);
                // $("#redeem-game-bottom").addClass('redeem-form-hide');
                       
        
            // $("#data").append(data);
        },
        error: function(err) {
            console.log(err);
        }
    });
  })
</script>
   @if(\Session::has('kodeRedeem'))
        <script>
     $('html, body').animate({
        scrollTop: $("#kodeRedeemShow").offset().top
    }, 2000);
                 
        </script>
        @endif   

  <!--@if(!empty($data))-->
  <!--  <script>-->
  <!--   $('html, body').animate({-->
  <!--      scrollTop: $("#redeemProgress").offset().top-->
  <!--    }, 2000);-->
  <!--  </script>-->
  <!--@endif-->

@if (\Session::has('error'))
<script>
           
  swal("", "Maaf kuota hadiah telah mencapai batas maksimal. Silahkan memilih hadiah lain untuk ditukarkan.", "warning");
</script>
@endif
@endsection