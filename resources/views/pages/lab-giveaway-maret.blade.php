@extends('layouts.app')

@section('cssonsite')
  <style media="screen">
  .big{
    font-size: 80px;
  }
  </style>
@endsection

@section('content')
  <div class="container">
      <h3 class="mt-5">LATIHAN Undian Giveaway Ultra Premium 8 Maret 2021 21:01:00 WIB</h3>
      <div id="undianGiveawayUpMaret">
      </div>
      <div>
        <p>daftar invoice per {{date('d-m-Y H:i:s',strtotime('+7 hours'))}} WIB</p>
        <div class="row">
          <div class="col-md-12">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">No Invoice</th>
                  <th scope="col">Nama</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($giveaway as $m => $value)
                  <tr>
                    <td>INV/...***{{$value['kode']}}</td>
                    <td>{{substr($value['nama'],0,4)}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>

      </div>
    </div>
@endsection

@section('js')
  <script type="text/javascript">
    $(document).ready(function(){
      setInterval(function() {
        $.ajax({
          type:"GET",
          url: "/lab/ajax/promo/giveaway/ultra-premium/maret",

        })
        .done(function(data)
        {
          if(data.html == ""){
            document.getElementById("undianGiveawayUpMaret").innerHTML = "Undian belum dimulai";
          }
          document.getElementById("undianGiveawayUpMaret").innerHTML = data.html;
        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {

        });
      }, 3000);


    });
  </script>
@endsection
