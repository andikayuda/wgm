    @extends('layouts.app')

    @section('content')
        <div>
            {{-- BANNER SECTION --}}
            <div class="hero">
                <div class="marquee">
                    <div class="hero-txt">
                        <h1>Warisan Gajahmada lagi bagi-bagi hadiah nih!</h1> <br>
                        <a href="{{ route('prize') }}" class="hero-btn">Pelajari Lebih Lanjut</a>
                    </div>
                    <div class="track">
                        <img class="marquee-img" src="{{ url('images/marquee/ps5.png') }}" alt="">
                        <img class="marquee-img" src="{{ url('./images/marquee/sepeda.png') }}" alt="">
                        <img class="marquee-img" src="{{ url('./images/marquee/iphone.png') }}" alt="">
                        <img class="marquee-img" src="{{ url('./images/marquee/ps5.png') }}" alt="">
                        <img class="marquee-img" src="{{ url('./images/marquee/sepeda.png') }}" alt="">
                        <img class="marquee-img" src="{{ url('./images/marquee/iphone.png') }}" alt="">
                    </div>
                </div>
            </div>

            {{-- PROMOSI SECTION --}}
            <div class="promosi container">
                <!--<h1>Warisan Maret 2021</h1>-->
                <h1 id="wgm_header"></h1>

                <div class="bagi-hadiah">
                    <img src="{{ URL::asset('images/banner-mei.jpg') }}" alt="" style="margin-top: 0">
                </div>

                <!--<div class="bagi-hadiah">-->
                <!--    <a href="{{ route('prize') }}">-->
                <!--        <img src="{{ URL::asset('images/KV.png') }}" alt="">-->
                <!--    </a>-->
                <!--</div>-->

                <div class="bagi-hadiah">
                    <a href="{{ route('koin') }}"><img src="{{ URL::asset('images/banner.jpg') }}" alt=""
                            style="margin-top: 0"></a>
                </div>

            </div>

            {{-- PRODUCT SECTION --}}
            <div class="produk" id="shopping-list">
                <h1><span>Produk</span> Warisan Gajahmada</h1>
                <div class="produk-card container">

                    {{-- <div class="produk-search">
                    <input class="form-control input-search" type="text" placeholder="Search...">
                </div> --}}



                    <div class="row rowgap" id="productSection">
                        @foreach ($products as $product)
                            @php
                                $image = unserialize($product->productImages);
                            @endphp
                            <div class="col-6 col-sm-6 col-md-4 ">
                                <div class="card produk-card-item">
                                    <div class="card-body">
                                        <img src="https://gudang.warisangajahmada.com/{{ $image[0] }}"
                                            class="produk-card-img" alt="">
                                        <p>{{ $product->productName }}</p>
                                        <h5>Rp. {{ number_format($product->productPrice) }},-</h5>
                                        <a href="{{ route('addToCart', $product->productSlug) }}"> <button
                                                class="produk-card-btn">Pesan Sekarang</button> </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    {{ $products->links() }}

                </div>
            </div>

            <div class="container mb-5">
                <div class="row tukar-koin" style="row-gap: 20px">
                    <div class="col-md-6">
                        <div class="card promosi-card">
                            <div class="card-body">
                                <h4>Tukar Koin</h4>
                                <select class="form-select tukar-koin-select"
                                    aria-placeholder="----Pilih Hadiah Redeem----">
                                    <option disabled selected>----Pilih Hadiah Redeem----</option>
                                    <option value="iphone">iPhone</option>
                                    <option value="ps5">PS5</option>
                                    <option value="sepeda">Sepeda</option>
                                    <option value="pulsa">Pulsa</option>
                                    <option value="ff">Free Fire Diamond</option>
                                    <option value="mole">Mobile Legends Diamond</option>
                                    <option value="tokped">Voucher Belanja Tokopedia</option>
                                </select>
                                <a href="#" type="button" class="lacak-btn text-center">TUKAR</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card promosi-card">
                            <div class="card-body">

                                <h4>Lacak Status</h4>
                                <input class="form-control axd" type="text" placeholder="Kode Redeem" id="lacakStatus"
                                    name="kode">
                                <button type="submit" class="lacak-btn text-center">LACAK</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- MODAL BERHASIL ADD TO CART --}}
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered ">
                    <div class="modal-content modal-border">
                        <div class="modal-header">
                            {{-- <h5 class="modal-title" id="exampleModalLabel">Modal title</h5> --}}
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" style="padding-top: 0">
                            <h3 class="modal-cart-head">Produk berhasil ditambahkan ke keranjang</h3>
                            <div class="card" style="border: transparent">
                                <div class="card-body modal-addtocart">
                                    <img src="{{ url('./images/produk-1.png') }}" class="modal-produk-img" alt="">
                                    <div class="modal-cart-desc">
                                        <p>Larutan Penyegar Cap Badak - Bundle 6
                                            Rasa Jambu</p>
                                        <h3>Rp. 48.500,-</h3>
                                    </div>

                                </div>
                            </div>
                            <button href="{{ route('cart') }}" class="to-cart-btn">LIHAT KERANJANG</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- ==================================================================================================================== --}}

        </div>
    @endsection

    @section('js')

        <script>
            $(".tukar-koin-select").on('click', function() {
                $('.tukar-koin-select').css('color', 'black')
            })
        </script>

        @if (\Session::has('success'))
            <script>
                $('html, body').animate({
                    scrollTop: $("#productSection").offset().top
                }, 200);
            </script>
            <script>
                swal("", "Produk berhasil ditambahkan ke keranjang!", "success");
            </script>
        @endif

        <script>
            $(document).ready(function() {

                const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
                    "Juli", "Agustus", "September", "Oktober", "November", "Desember"
                ]

                const dateObj = new Date()
                const monthNumber = dateObj.getMonth()
                const monthName = monthNames[monthNumber]

                $("#wgm_header").html(`Warisan ${monthName} 2021`);

            });
        </script>


    @endsection
