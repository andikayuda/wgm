@extends('layouts.app')

@section('cssonsite')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">

  <style media="screen">
  .big{
    font-size: 60px;
  }
  .mb-6{
    margin-bottom: 6px;
  }
  .mb-20{
    margin-bottom: 20px;
  }
  </style>
@endsection

@section('content')
  <div class="container">
      <h3 class="mt-5">Undian GEBYAR GIVEAWAY 24 Mei 2021 16:30:00 WIB</h3>
      <hr>
      <div id="undianGebyarPengundian">
        <div class="row">
          <div class="col-md-4" id="undianGebyarPengundianPS5">
            <div class="card mb-20">
              <div class="card-header text-center">
                <h4>Hasil Undian PS5</h4>
              </div>
              <div class="card-body">

              </div>
              <div class="card-footer">

              </div>
            </div>
          </div>
          <div class="col-md-4" id="undianGebyarPengundianRealme">
            <div class="card mb-20">
              <div class="card-header text-center">
                <h4>Hasil Undian HP Realme</h4>
              </div>
              <div class="card-body">

              </div>
              <div class="card-footer">

              </div>
            </div>
          </div>
          <div class="col-md-4" id="undianGebyarPengundianIphone">
            <div class="card mb-20">
              <div class="card-header text-center">
                <h4>Hasil Undian Iphone 12 Pro</h4>
              </div>
              <div class="card-body">

              </div>
              <div class="card-footer">

              </div>
            </div>
          </div>
          <div class="col-md-4" id="undianGebyarPengundianDiamond">
            <div class="card mb-20">
              <div class="card-header text-center">
                <h4>Hasil Undian Diamond</h4>
              </div>
              <div class="card-body">

              </div>
              <div class="card-footer">

              </div>
            </div>
          </div>
          <div class="col-md-4" id="undianGebyarPengundianSepeda">
            <div class="card mb-20">
              <div class="card-header text-center">
                <h4>Hasil Undian Sepeda</h4>
              </div>
              <div class="card-body">

              </div>
              <div class="card-footer">

              </div>
            </div>
          </div>
        </div>
      </div>
      <hr>
      <h3 class="mt-5">DAFTAR PESERTA</h3>
      <div class="row">
        <div class="col-md-12">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            @foreach ($data as $n => $isi)
              <li class="nav-item">
                <a class="nav-link {{$n==0 ? 'active' : ''}}" id="{{$isi['slug']}}-tab" data-toggle="tab" href="#{{$isi['slug']}}" role="tab" aria-controls="{{$isi['slug']}}" aria-selected="true">{{$isi['category']}}</a>
              </li>
            @endforeach
          </ul>
          <div class="tab-content" id="myTabContent">
            @foreach ($data as $n => $isi)
              <div class="tab-pane fade {{$n==0 ? 'show active' : ''}} " id="{{$isi['slug']}}" role="tabpanel" aria-labelledby="{{$isi['slug']}}-tab">
                <div class="card mb-20">
                  <div class="card-header text-center">
                    <h4>DAFTAR UNDIAN {{strtoupper($isi['category'])}}</h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table" id="myTable{{$isi['slug']}}">
                        <thead class="thead-dark">
                          <tr>
                            <th scope="col">Kupon</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Kode Resi</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($isi['peserta'] as $kupon)
                            <tr>
                              <th scope="row">{{$kupon->kodeUndian}}</th>
                              <td>{{$kupon->nama}}</td>
                              <td>{{$kupon->kode_form}}</td>
                            </tr>
                          @endforeach


                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="card-footer text-muted">

                  </div>
                </div>
              </div>
            @endforeach

          </div>
        </div>

      </div>
    </div>
@endsection

@section('js')
  <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

  <script type="text/javascript">
      $(document).ready( function () {
        @foreach ($data as $n => $isi)
          $('#myTable{{$isi['slug']}}').DataTable();
        @endforeach

        setInterval(function() {
          $.ajax({
            type:"GET",
            url: "/ajax/promo/giveaway/gebyar-giveaway-mei-2021/ps5",

          })
          .done(function(data)
          {
            if(data.html == ""){
              document.getElementById("undianGebyarPengundianPS5").innerHTML = "Undian belum dimulai";
            }
            document.getElementById("undianGebyarPengundianPS5").innerHTML = data.html;
          })
          .fail(function(jqXHR, ajaxOptions, thrownError)
          {

          });

          $.ajax({
            type:"GET",
            url: "/ajax/promo/giveaway/gebyar-giveaway-mei-2021/iphone",

          })
          .done(function(data)
          {
            if(data.html == ""){
              document.getElementById("undianGebyarPengundianIphone").innerHTML = "Undian belum dimulai";
            }
            document.getElementById("undianGebyarPengundianIphone").innerHTML = data.html;
          })
          .fail(function(jqXHR, ajaxOptions, thrownError)
          {

          });

          $.ajax({
            type:"GET",
            url: "/ajax/promo/giveaway/gebyar-giveaway-mei-2021/sepeda",

          })
          .done(function(data)
          {
            if(data.html == ""){
              document.getElementById("undianGebyarPengundianSepeda").innerHTML = "Undian belum dimulai";
            }
            document.getElementById("undianGebyarPengundianSepeda").innerHTML = data.html;
          })
          .fail(function(jqXHR, ajaxOptions, thrownError)
          {

          });

          $.ajax({
            type:"GET",
            url: "/ajax/promo/giveaway/gebyar-giveaway-mei-2021/realme",

          })
          .done(function(data)
          {
            if(data.html == ""){
              document.getElementById("undianGebyarPengundianRealme").innerHTML = "Undian belum dimulai";
            }
            document.getElementById("undianGebyarPengundianRealme").innerHTML = data.html;
          })
          .fail(function(jqXHR, ajaxOptions, thrownError)
          {

          });

          $.ajax({
            type:"GET",
            url: "/ajax/promo/giveaway/gebyar-giveaway-mei-2021/diamond",

          })
          .done(function(data)
          {
            if(data.html == ""){
              document.getElementById("undianGebyarPengundianDiamond").innerHTML = "Undian belum dimulai";
            }
            document.getElementById("undianGebyarPengundianDiamond").innerHTML = data.html;
          })
          .fail(function(jqXHR, ajaxOptions, thrownError)
          {

          });
        }, 10000);

      } );
    </script>


@endsection
