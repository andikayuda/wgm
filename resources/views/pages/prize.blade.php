@extends('layouts.app')

@section('content')
    <section class="container-fluid">
      <div class="prize-header">
        <div class="prize-header-img-wrapper">
          <img src="{{URL::asset('images/koin_prize.png')}}" alt="">
        </div>
        <p>
          Ayo buruan beli Larutan Penyegar Cap Badak di
          <br> 
          Warisan Gajahmada dan raih hadiahnya!
        </p>
      </div>
      <div class="content container">
        <span>Cara</span>
        <span>Mendapatkan</span>
        <div class="step1">
          <div class="row">
            <div class="col-md-4 prize-image-wrapper">
              <img src="{{URL::asset('images/prize_step_1.png')}}" alt="" class="prize-step-image1">
            </div>
            <div class="col-md-8 prize-step-1-wrapper" >
              <div class="row prize-step-1">
                <div class="col-1 prize-step-numbers">
                  <h1>1.</h1>
                </div>
                <div class="col-10 prize_step_text">
                  <span>Beli</span>
                  <span>Larutan Penyegar</span>
                  <br>
                  <span>Cap Badak</span>
                  <p>Beli paket Bundle isi 6 pcs kaleng Larutan Penyegar Cap Badak yang sudah berisi Koin Gatotkaca.</p>
                  <a class="btn btn-danger" href="{{route('home').'/#shopping-list'}}" role="button">PESAN SEKARANG</a> 
                </div>
              </div>                       
            </div>
          </div>
        </div>
        <div class="step2">
          <div class="row prize-step-2 justify-content-center">
            <div class="col-1">
              <h1 class="number-step-1">2.</h1>
            </div>
            <div class="col-10 prize_step_text-2">
              <span>Kumpulkan</span>
              <span>Koin Gatotkaca</span>
              <p>Kumpulkan koleksi Koin Gatotkaca mu sebanyak-banyaknya dan raih hadiah yang kamu ingingkan!</p>
            </div>
          </div>
        </div>
        <div class="prize-item-card">
          <h2 class="text-center my-5">Hadiah yang dapat kamu tukarkan</h2>
          <div class="row">
            @foreach ($prizeSingle as $item)
            @if ($item->slug == "iphone-12-pro" || $item->slug == "ps5" || $item->slug == "sepeda" || $item->slug == "pulsa-200k" || $item->slug == "hp-android")
              <div class="col-md-3 col-6" style="margin-top: 25px">
                <div class="grid-inside">
                  <img src="{{$item->photoCarousel}}" alt="{{$item->brand}}" class="prize-img-new prize-{{ $item->slug }}">
                  <img src="{{$item->mobileCoin}}" alt="{{$item->brand}}_koin" class="prize-{{$item->slug}}_koin">
                  <button type="button" class="btn btn-prize-detail" id="{{$item->slug}}Button" data-toggle="modal" data-target="#koin-modal-{{$item->slug}}">LIHAT DETAIL</button>
                  <div class="container kuota-container">
                    <div class="row">
                      <div class="col-9">
                        <p class="kuota-text">Kuota Tersedia</p></div>
                      <div class="col-3"><p class="kuota-text">{{$item->kuotaAwal}}</p></div>
                    </div>
                    <div class="row">
                      <div class="col-9">
                        <p class="kuota-text">Kuota Tersisa</p>
                      </div>
                      <div class="col-3"><p class="kuota-text">{{$item->kuotaPenukaran}}</p></div>
                    </div>
                  </div> 
                </div>
              </div>
              @endif
            @endforeach

            @foreach ($prizeSingle as $item)
              @if ($item->slug == "diamond-ff" || $item->slug == "diamond-ml" || $item->slug == "tokopedia" || $item->slug == "uang-elektronik")
                <div class="col-md-3 col-6" style="margin-top: 25px">
                  <div class="grid-inside">
                    <img src="{{$item->photoCarousel}}" alt="ML" class="prize-mole">
                    <button type="button" class="btn btn-prize-detail-game" id="{{$item->slug}}Button" data-toggle="modal" data-target="#koin-modal-{{$item->slug}}">LIHAT DETAIL</button>
                  </div>
                </div>
              @endif
            @endforeach
            
          </div>
        </div>
        {{-- <div class="prize-item-card " style="margin-top: 6vh;">
          <div class="row">
            @foreach ($prizeSingle as $item)
                @if ($item->slug == "diamond-ff" || $item->slug == "diamond-ml" || $item->slug == "tokopedia" || $item->slug == "uang-elektronik")
                  <div class="col-md-3 col-6">
                    <div class="grid-inside">
                      <img src="{{$item->photoCarousel}}" alt="ML" class="prize-mole">
                      <button type="button" class="btn btn-prize-detail-game" id="{{$item->slug}}Button" data-toggle="modal" data-target="#koin-modal-{{$item->slug}}">LIHAT DETAIL</button>
                    </div>
                  </div>
                @endif
            @endforeach
          
          <div class="col-md-3 col-6"></div>
          </div>
        </div> --}}
        <div class="game-cash-prize">
              @foreach ($prizeGame as $items)
              {{-- @php
              var_dump($items[0]->tag);
              @endphp --}}
              <table class="table table-borderless table-koin-{{$items[0]->tag}}">
                <thead>
                  <tr>
                    <th scope="col" class="table-title-1">Jumlah Diamond</th>
                    <th scope="col-" class="table-title-2">Koin yang dibutuhkan</th>
                    <th scope="col" class="table-title-3">Koin tersisa</th>
                  </tr>
                </thead>
                  @foreach ($items as $item)
                 
                    <tbody>
                      <tr>
                        <th>{{$item->prize}}</th>
                        @if ($item->prize == "Uang Elektronik 1jt")
                            <td><img src="{{$item->imageWeb}}" alt="" class="prize-image-table-2"></td>
                        @else
                            <td><img src="{{$item->imageWeb}}" alt="" class="prize-image-table"></td>
                        @endif
                        <td>{{$item->kuotaPenukaran}}</td>
                      </tr>
                      <tr>
                    </tbody>
                    @endforeach
                  </table>
              @endforeach
              
          {{-- <span>Jumlah Diamond</span>
          <span>Koin yang dibutuhkan</span> --}}
          {{-- <img src="{{URL::asset('images/free-fire-1.png')}}" alt=""> --}}
        </div>
        <div class="step3 justify-content-center">
          <div class="row">
            <div class="col-md-8 step3-text">
              <div class="row prize-step-3">
                <div class="col-2 prize-step-numbers-3">
                  <h1>3.</h1>
                </div>
                <div class="col-10 prize_step_text-3">
                  <span>Tukarkan</span>
                  <span>Koin Gatotkaca</span>
                  <p>Setelah kamu mengumpulkan semua koinnya, tukarkan koinmu disini</p>
                 
                  <a class="btn btn-danger" href="#" role="button" disabled >TUKAR SEKARANG</a>            
                </div>
              </div>
            </div>
            <div class="col-md-4 img-step-3-wrapper">
              <img src="{{URL::asset('images/prize_step_1.png')}}" alt="">
            </div>
          </div>
        </div>
      </div>

      @foreach ($prizeSingle as $item)
        @if ($item->slug == "iphone-12-pro" || $item->slug == "ps5" || $item->slug == "sepeda" || $item->slug == "pulsa-200k" || $item->slug= "hp-android")
            <div class="modal" id="koin-modal-{{$item->slug}}" tabindex="-1">
              <div class= "modal-dialog modal-dialog-centered" >
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <h1>Koin yang harus ditukarkan</h1>
                    <img src="{{$item->mobileCoin}}" alt="iphone_koin" class="prize-mobile-{{$item->slug}}_koin">
                  </div>
                </div>
              </div>
            </div>
        @endif
      @endforeach
      
      {{-- modal ff--}}
      <div class="modal" id="koin-modal-diamond-ff" tabindex="-1">
        <div class= "modal-dialog modal-dialog-centered" >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <h1>Koin yang harus ditukarkan</h1>
              <div class="swiper-modal-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond FF <b>1jt</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ff1jt.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond FF <b>500rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ff500rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond FF <b>300rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ff300rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond FF <b>200rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ff200rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond FF <b>100rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ff100rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond FF <b>50rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ff50rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond FF <b>20rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ff20rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {{-- modal mole--}}
      <div class="modal" id="koin-modal-diamond-ml" tabindex="-1">
        <div class= "modal-dialog modal-dialog-centered" >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <h1>Koin yang harus ditukarkan</h1>
              <div class="swiper-modal-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond ML <b>1jt</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ml1jt.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond ML <b>500rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ml500rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond ML <b>300rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ml300rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond ML <b>165rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ml165rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond ML <b>120rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ml120rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond ML <b>88rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ml88rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond ML <b>71,5rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ml71,5rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond ML <b>49rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ml49rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {{-- modal tokped--}}
      <div class="modal" id="koin-modal-tokopedia" tabindex="-1">
        <div class= "modal-dialog modal-dialog-centered" >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <h1>Koin yang harus ditukarkan</h1>
              <div class="swiper-modal-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide swiper-no-border">
                    <h1>Voucher Belanja <b>1jt</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_tokped1jt.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Voucher Belanja <b>500rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_tokped500rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Voucher Belanja <b>250rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_tokped250rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Voucher Belanja <b>150rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_tokped150rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      {{-- modal ewallet--}}
      <div class="modal" id="koin-modal-uang-elektronik" tabindex="-1">
        <div class= "modal-dialog modal-dialog-centered" >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <h1>Koin yang harus ditukarkan</h1>
              <div class="swiper-modal-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide swiper-no-border">
                    <h1>Uang Elektronik <b>1jt</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/ewallet_emas_mobile.png')}}" alt="ewallet_koin" class="prize-mobile-ewallet_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Uang Elektronik <b>500rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/e_wallet_500rb_mobile.png')}}" alt="ewallet_koin" class="prize-mobile-ewallet_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Voucher Belanja <b>150rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/e_wallet_150rb.png')}}" alt="ewallet_koin" class="prize-mobile-ewallet_koin"> 
                    </div>
                  </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection

@section('js')


<script src="{{ URL::asset('js/swiper-bundle.min.js') }}"></script>

<script>
  $(document).ready(function() {
    $('#koin-modal-diamond-ff').on('shown.bs.modal', function(e) {
      var swiperPrize = new Swiper('.swiper-modal-container', {
      slidesPerView: 1,
      // slidesPerColumn: 1,
      spaceBetween: 10,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        425: {
          slidesPerView: 1,
          spaceBetween: 10,
        }
      }
    });
    })

    // mole
    $('#koin-modal-diamond-ml').on('shown.bs.modal', function(e) {
      var swiperPrizeMole = new Swiper('.swiper-modal-container', {
      slidesPerView: 1,
      // slidesPerColumn: 1,
      spaceBetween: 10,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        425: {
          slidesPerView: 1,
          spaceBetween: 10,
        }
      }
    });
    })
    $('#koin-modal-tokopedia').on('shown.bs.modal', function(e) {
      var swiperPrizeTokped = new Swiper('.swiper-modal-container', {
      slidesPerView: 1,
      // slidesPerColumn: 1,
      spaceBetween: 10,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        425: {
          slidesPerView: 1,
          spaceBetween: 10,
        }
      }
    });
    })
    $('#koin-modal-uang-elektronik').on('shown.bs.modal', function(e) {
      var swiperPrizeEwallet = new Swiper('.swiper-modal-container', {
      slidesPerView: 1,
      // slidesPerColumn: 1,
      spaceBetween: 10,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        425: {
          slidesPerView: 1,
          spaceBetween: 10,
        }
      }
    });
    })
  })
  
</script>

<script>
            
  $('#iphone-12-proButton, #ps5Button, #sepedaButton, #pulsa-200kButton, #hp-androidButton').click(function () {
      var widthWindows = $(window).width();
      // console.log(widthWindows);
      if(widthWindows > 425) {
          $('#iphone-12-proButton, #ps5Button, #sepedaButton, #pulsa-200kButton, #hp-androidButton').removeAttr('data-toggle');
          
          if (this.id == 'iphone-12-proButton') {
              $("#iphone-12-proButton, .prize-iphone-12-pro").addClass('prize-opacity');
              // $(".prize-iphone").addClass('prize-opacity');
              $(".prize-iphone-12-pro_koin").css('display', 'block');
              $("#ps5Button, #sepedaButton, #pulsa-200kButton, #hp-androidButton, .prize-ps5, .prize-sepeda, .prize-pulsa-200k, .prize-hp-android").removeClass('prize-opacity');
              $(".prize-ps5_koin, .prize-sepeda_koin, .prize-pulsa-200k_koin, .prize-hp-android_koin").css('display', 'none');
          }
          else if (this.id == 'ps5Button') {
              $("#ps5Button, .prize-ps5").addClass('prize-opacity');
              $(".prize-ps5_koin").css('display', 'block');
              $("#iphone-12-proButton, #sepedaButton, #pulsa-200kButton, #hp-androidButton, .prize-iphone-12-pro, .prize-sepeda, .prize-pulsa-200k, .prize-hp-android").removeClass('prize-opacity');
              $(".prize-iphone-12-pro_koin, .prize-sepeda_koin, .prize-pulsa-200k_koin, .prize-hp-android_koin").css('display', 'none');
          }
          else if (this.id == 'sepedaButton') {
              $("#sepedaButton, .prize-sepeda").addClass('prize-opacity');
              $(".prize-sepeda_koin").css('display', 'block');
              $("#iphone-12-proButton, #ps5Button, #pulsa-200kButton, #hp-androidButton, .prize-iphone-12-pro, .prize-ps5, .prize-pulsa-200k, .prize-hp-android").removeClass('prize-opacity');
              $(".prize-iphone-12-pro_koin, .prize-ps5_koin, .prize-pulsa-200k_koin, .prize-hp-android_koin").css('display', 'none');
          }
          else if (this.id == 'pulsa-200kButton') {
              $("#pulsa-200kButton, .prize-pulsa-200k").addClass('prize-opacity');
              $(".prize-pulsa-200k_koin").css('display', 'block');
              $("#iphone-12-proButton, #ps5Button, #sepedaButton, #hp-androidButton, .prize-iphone-12-pro, .prize-ps5, .prize-sepeda, .prize-hp-android").removeClass('prize-opacity');
              $(".prize-iphone-12-pro_koin, .prize-ps5_koin, .prize-sepeda_koin, .prize-hp-android_koin").css('display', 'none');
          }
          else if (this.id == 'hp-androidButton') {
              $("#hp-androidButton, .prize-hp-android").addClass('prize-opacity');
              $(".prize-hp-android_koin").css('display', 'block');
              $("#iphone-12-proButton, #ps5Button, #sepedaButton, .prize-iphone-12-pro, .prize-ps5, .prize-sepeda").removeClass('prize-opacity');
              $(".prize-iphone-12-pro_koin, .prize-ps5_koin, .prize-sepeda_koin").css('display', 'none');
          }
      }else if (widthWindows <= 425) {
          if (this.id == 'iphone-12-proButton') {
              $('#iphone-12-proButton').attr('data-toggle', 'modal');
          }else if (this.id == 'ps5Button') {
              $('#ps5Button').attr('data-toggle', 'modal');
          }else if (this.id == 'sepedaButton') {
              $('#sepedaButton').attr('data-toggle', 'modal');
          }else if (this.id == 'pulsa-200kButton') {
              $('#pulsa-200kButton').attr('data-toggle', 'modal');
          }else if (this.id == 'hp-androidButton') {
              $('#hp-androidButton').attr('data-toggle', 'modal');
          }
      }
  });
</script>
<script>
  $('#diamond-ffButton, #diamond-mlButton, #tokopediaButton, #uang-elektronikButton').click(function () {
      var widthWindows = $(window).width();

      if(widthWindows > 425) {
        $('#diamond-mlButton, #tokopediaButton, #diamond-ffButton, #uang-elektronikButton').removeAttr('data-toggle');
          if(this.id === 'diamond-ffButton') {
              $('.table-koin-ml, .table-koin-tokped, .table-koin-ewallet').css('display', 'none');
              $('#diamond-mlButton, #tokopediaButton, #uang-elektronikButton').removeClass('btn-prize-red');
              $('.game-cash-prize, .table-koin-ff').css('display', 'block');
              $('#diamond-ffButton').addClass('btn-prize-red')
          }else if(this.id === 'diamond-mlButton') {
              $('.table-koin-ff, .table-koin-tokped, .table-koin-ewallet').css('display', 'none');
              $('#diamond-ffButton, #tokopediaButton, #uang-elektronikButton').removeClass('btn-prize-red');
              $('.game-cash-prize, .table-koin-ml').css('display', 'block');
              $('#diamond-mlButton').addClass('btn-prize-red')
          }else if(this.id === 'tokopediaButton') {
              $('.table-koin-ff, .table-koin-ml, .table-koin-ewallet').css('display', 'none');
              $('#diamond-ffButton, #diamond-mlButton, #uang-elektronikButton').removeClass('btn-prize-red');
              $('.game-cash-prize, .table-koin-tokped').css('display', 'block');
              $('#tokopediaButton').addClass('btn-prize-red')
          }else if(this.id === 'uang-elektronikButton') {
              $('.table-koin-ff, .table-koin-ml, .table-koin-tokped').css('display', 'none');
              $('#diamond-ffButton, #diamond-mlButton, #tokopediaButton').removeClass('btn-prize-red');
              $('.game-cash-prize, .table-koin-ewallet').css('display', 'block');
              $('#uang-elektronikButton').addClass('btn-prize-red')
          }
      }else if(widthWindows <= 425) {
          if(this.id === 'diamond-ffButton') {
            $('#diamond-ffButton').attr('data-toggle', 'modal');
          }else if(this.id === 'diamond-mlButton') {
            $('#diamond-mlButton').attr('data-toggle', 'modal');
          }else if(this.id === 'tokopediaButton') {
            $('#tokopediaButton').attr('data-toggle', 'modal');
          }else if (this.id === 'uang-elektronikButton') {
            $('#uang-elektronikButton').attr('data-toggle', 'modal');
          }
      }
      
  })
</script>

    
@endsection

