<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Warisan Gajahmada</title>

    {{-- FAVICON --}}
    <link rel="icon" type="image/png" href="{{ url('images/favicon/favicon-wgm.png') }}">

    <link rel="stylesheet" href="{{ URL::to('css/form-leaderboard.css') }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css"
        integrity="sha512-8bHTC73gkZ7rZ7vpqUQThUDhqcNFyYi2xgDgPDHc+GXVGHXq+xPjynxIopALmOPqzo9JZj0k6OqqewdGO3EsrQ=="
        crossorigin="anonymous" />
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous" /> --}}
        <link rel="stylesheet" href="{{url('/')}}/vendor/crudbooster/assets/sweetalert/dist/sweetalert.css">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body>
    <div class="form_container">
        <div class="input_container">
            <h1><span>Isi Data</span> Berikut</h1>

            <!-- {{-- <div class="row_input">
                <div class="ui fluid input" style="width: 100%">
                    <input type="text" placeholder="Tiktok Account" class="user_input">
                </div>
            </div>

            <div class="row_input">
                <div class="ui fluid icon input" style="width: 100%">
                    <input type="text" placeholder="NIK Karyawan" class="user_input">
                    <i class="check icon" style="color: #10C78B"></i>
                </div>
                <div class="ui button check">CHECK</div>
            </div> --}} -->

            <form id="bubu" action="{{route('inputCompetition')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="ui form">
                    <div class="field">
                        <input type="text" placeholder="Tiktok Account" class="user_input" id="tiktok_acc"
                            name="tiktokUsername" required>
                    </div>

                    <div class="fields">
                        <div class="twelve wide field">
                            <div class="ui fluid icon input" id="INPUT_NIK">
                                <input type="text" placeholder="NIK Karyawan" class="user_input" id="NIK"
                                    onchange="rmvCls()" name="nik">
                                <i class="icon" id="input_icon" required></i>
                            </div>
                        </div>
                        <div class="four wide field">
                            <div class="ui button check" onclick="checkNIK()">CHECK</div>
                        </div>
                    </div>

                    <div class="field">
                        <input type="text" placeholder="#Main_Hashtag" class="user_input" id="hashtag"
                            name="mainHashtag" required>
                    </div>

                    <div class="fields">

                        <div class="ten wide field">
                            <div class="ui fluid transparent input">
                                <input type="text" placeholder="Upload bukti pembelianmu disini" readonly
                                    class="user_input">
                                <input type="file" id="user_file" name="buktiPembelian" required>
                            </div>
                        </div>

                        <div class="six wide field">
                            <div class="ui button upload">
                                UPLOAD FILE
                            </div>
                        </div>


                    </div>
                    <div class="ui button submit" id="disclaimerModal">SUBMIT</div>
                     <!-- {{-- MODAL --}} -->
                    <div class="ui modal">
                        <div class="content">
                            <div class="description modal_content">
                                <h4>Disclaimer</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus id nullam nulla parturient
                                    malesuada. Nunc integer erat interdum eu elementum pellentesque aenean sit amet. Quam amet dolor
                                    pharetra lorem integer velit. Quam sed aliquam volutpat vestibulum dolor libero. </p>
                                <button type="submit" id="submitButton" class="ui button submit">Continue</button>
                            </div>
                        </div>
                    </div>
                    <!-- ENd modal -->
                </div>
            </form>


            <!-- {{-- <form>
                <input type="text" class="form-control user_input" placeholder="Tiktok Account" />

                <input type="text" class="form-control user_input" placeholder="NIK Karyawan" />

                <input type="text" class="form-control user_input" placeholder="#Main_Hashtag" />

                <button class="btn">Upload Bukti Pembelian</button>

                <button type="submit" class="btn">Submit</button>
            </form> --}} -->
        </div>

       
    </div>



    <!-- {{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script> --}} -->
    <script src="https://code.jquery.com/jquery-3.6.0.js"
        integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.js"
        integrity="sha512-dqw6X88iGgZlTsONxZK9ePmJEFrmHwpuMrsUChjAw1mRUhUITE5QU9pkcSox+ynfLhL15Sv2al5A0LVyDCmtUw=="
        crossorigin="anonymous"></script>
        <script src="{{url('/')}}/vendor/crudbooster/assets/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        // Get File Name
        // $("input:text").click(function() {
        //     $(this).parent().find("input:file").click();
        // });

        // $('input:file', '.ui.input')
        //     .on('change', function(e) {
        //         var name = e.target.files[0].name;
        //         $('input:text', $(e.target).parent()).val(name);
        //         console.log(e.target.files[0])
        //     });

        // Check NIK
        $("#NIK").focusout(checkNIK);

        // CHECK NIK FUNCTION
        function checkNIK() {
            var str = $("#NIK").val();
            console.log(str)

            $("#INPUT_NIK").toggleClass("loading")

            $.ajax({
                type: 'GET',
                url: `https://contest-tiktok-sinde.yokesen.com/api/karyawan?nik=${str}`,
                success: function(data) {
                    if (data.status === 'success') {
                        // $("#INPUT_NIK").append('<i class="check icon" style="color: #10C78B"></i>')
                        $("#INPUT_NIK").toggleClass("loading")
                        $("#input_icon").addClass("check")
                    } else if (data.status === 'failed') {
                        // $("#INPUT_NIK").append('<i class="times icon" style="color: red"></i>')
                        $("#INPUT_NIK").toggleClass("loading")
                        $("#input_icon").addClass("times")
                    }
                    console.log(data)
                },
            });
        }

        function rmvCls() {
            $("#input_icon").removeClass("times")
            $("#input_icon").removeClass("check")
        }

        // MODAL
        $(function() {
            var bukti = ""
            $("input:text").click(function() {
                $(this).parent().find("input:file").click();
            });

            $('input:file', '.ui.input')
                .on('change', function(e) {
                    var name = e.target.files[0].name;
                    $('input:text', $(e.target).parent()).val(name);
                    bukti = e.target.files[0]
                    // console.log(bukti)
                });

          
        });
        $("#disclaimerModal").click(function(){
            console.log('cek')
            $('.ui.modal').modal('show');
            // $(".ui.modal").modal({
            //     closable: true
            // });

        });
        $("#submitButton").click(function(){
            $("#bubu").submit();
        });
       

    </script>
    @if (\Session::has('success'))
            <script>
            
                swal("", "Data berhasil ditambahkan!", "success");
            </script>
    @endif
    @if (\Session::has('error'))
            <script>
            
                swal("", "Data gagal ditambahkan!", "error");
            </script>
    @endif
    @if (\Session::has('username'))
            <script>
            
                swal("", "Data gagal ditambahkan! Username sudah terdaftar", "error");
            </script>
    @endif
</body>

</html>
