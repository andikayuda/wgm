@extends('layouts.app')

@section('content')

<div class="container">
    <h3 class="mt-5">Keranjang</h3>
    <div class="cart">
    @foreach (Cart::content()->sortBy('id') as $n => $cart)
        <div class="card">
            <div class="card-body cart-item">
                <img src="https://gudang.warisangajahmada.com/{{$cart->options['image']}}" class="cart-img" alt="product" >
                <div class="cart-item-desc">
                    <h4>{{$cart->name->productName}} <br>
                    {{$cart->name->productNote1}}</h4>
                    <h2>Rp {{number_format($cart->price,'2','.','.')}},-</h2>
                    <a href="{{route('removeFromCart',$cart->rowId)}}">Remove</a>
                </div>
                <div class="item-qty">
                    <a href="{{route('addQtyCart',$cart->rowId)}}" style="color: black"><i class="fas fa-plus"></i></a>
                    <h3 class="mt-2">{{$cart->qty}}</h3>
                    <a href="{{route('subQtyCart',$cart->rowId)}}" style="color: black"><i class="fas fa-minus"></i></a>
                </div>
            </div>
        </div>
    @endforeach
        
    </div>

    <div class="coupon">
        <div class="card">
            <div class="card-body">
              {{-- <h4>Masukkan Kode Kupon</h4>
              <div class="input-group cek-coupon">
                <input type="text" class="form-control" placeholder="Kode Kupon">
                <button class="cek-coupon-btn" type="button">CEK</button>
              </div> --}}

              <div class="row">
                  <div class="col-md-6">
                      {{-- <div class="coupon-subtotal my-3"> 
                          <h4>PPN 10%</h4>
                          <h4>Rp 142.200</h4>
                      </div> --}}
                      <div class="coupon-subtotal"> 
                        <h4>Total Belanja</h4>
                        <h2>Rp {{Cart::subtotal()}},-</h2>
                    </div>
                  </div>
                  <div class="col-md-6 btn-checkout-container">
                    @if (Cart::subtotal() <= 0)
                        <a href="" class="btn-disabled">CHECKOUT</a>
                    @else  
                        <a href="{{route('storeCart')}}" class="btn-checkout">CHECKOUT</a>  
                    @endif
                  </div>
              </div>
            </div>
          </div>
    </div>
    
</div>




@endsection

@section('js')
<script>
    // -----------------for-Shopping-cart-------------


    $('.minus-btn').on('click', function(e) {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest('div').find('h3');
    var value = parseInt($input.val());
    // var price = $this.closest('div').find('.product-prize').value;
    // console.log(price);
    if(value == 1){
        value = 1;
    }
    else{
        value = value - 1;
    }
  $input.val(value);
//   console.log("value"+ value)
    var id = $input.attr('id');
    var id = $input.attr('id');
    var id = 'minusButton'+id;
    // console.log(id);
    checkClass(value, id);
});
 
$('.plus-btn').on('click', function(e) {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest('div').find('input');
    var value = parseInt($input.val());
    // var price = $this.document.getElementsByClassName('product-prize');
    // console.log(price);
    value = value + 1;

    $input.val(value);
    var id = $input.attr('id');
    var id = 'minusButton'+id;
    // console.log(id);
    checkClass(value, id);
    // console.log("value"+ value)
});

function checkClass(value, id){
    console.log(value);
    console.log(id);
    // var input = document.getElementById(`${id}`).value;
    // var input = document.getElementById('0');
    // console.log(input);
    if(value <= 1 ){
        $(`#${id}`).addClass('opacity-30');
       
    }
    else{
        $(`#${id}`).removeClass('opacity-30');
    }
}

function changePrice(value, id){
    var data = value * id;
    return data;
}

</script>
@endsection