<footer class="container-fluid footer-wrapper">
  <div class="footer-web">
    <div class="row footer-web-wrapper">
      <div class="col-md-3">
        <div class="row ">
          <div class="col-6">
            <img src="{{URL::asset('images/WGM.png')}}" alt="">
          </div>
          <div class="col-6">
            <ul class="footer-list">
              <li><a href="#">Home</a></li>
              <li><a href="#">Prize</a></li>
              <li><a href="#">Koin Gatotkaca</a></li>
              <li><a href="#">Tukar Koin</a></li>
              <li><a href="#">Cart</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-md-3" style="border: 1px solid white"></div>
      <div class="col-md-6">
        <p>Temukan kami di</p>
        <div class="footer-tokped-button">
          <a href="" class="tokped-icon"><img src="{{URL::asset('images/tokped_icon.svg')}}" alt="" >Tokopedia</a>
        </div>
        <div class="footer-shopee-button">
          <a href="" class="shopee-icon"><img src="{{URL::asset('images/shopee_icon.svg')}}" alt="" >Shopee</a>
        </div>
        <div class="footer-blibli-button">
          <a href="" class="blibli-icon"><img src="{{URL::asset('images/blibli_icon.svg')}}" alt="" >Blibli</a>
        </div>
        <p>kontak kami</p>
        <div class="footer-wa-wrapper">
          <a href=""><img src="{{URL::asset('images/wa_icon.svg')}}" alt="">Whatsapp Business</a>
        </div>
      </div>
    </div>
    <hr style="border: 1px solid white;">
    <p class="footer-copyright">Copyright Warisan Gajahmada - All Right Reserved</p>
  </div>
  <div class="footer-mobile">
    <div class="row justify-content-between footer-mobile-wrapper">
      <div class="col-5 mobile-temukan">
        <p>Temukan kami di</p>
        <div class="footer-mobile-tokped">
          <a href=""><img src="{{URL::asset('images/tokped_icon.svg')}}" alt="">Tokopedia</a>
        </div>
        <div class="footer-mobile-shopee">
          <a href=""><img src="{{URL::asset('images/shopee_icon.svg')}}" alt="">Shopee</a>
        </div>
        <div class="footer-mobile-blibli">
          <a href=""><img src="{{URL::asset('images/blibli_icon.svg')}}" alt="">Blibli</a>
        </div>
      </div>
      <div class="col-6 mobile-hubungi">
        <p>Kontak kami</p>
        <div class="footer-mobile-wa">
          <a href=""><img src="{{URL::asset('images/wa_icon.svg')}}" alt="">Whatsapp Business</a>
        </div>
      </div>
    </div>
    <hr style="border: 1px solid white;">
    <div class="row justify-content-between align-items-end mobile-footer-below">
      <div class="col-3 footer-mobile-wgm">
        <img src="{{URL::asset('images/WGM.png')}}" alt="">
      </div>
      {{-- <div class="2">
      </div> --}}
      <div class="7 mobile-copyright">
        <p class="footer-mobile-copyright">Copyright Warisan Gajahmada <br> - All Right Reserved</p>

      </div>
    </div>
  </div>
</footer>