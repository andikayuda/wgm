<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">



    <title>Warisan Gajahmada</title>

    {{-- FAVICON --}}
    <link rel="icon" type="image/png" href="{{ url('images/favicon/favicon-wgm.png') }}">

    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '431618748055595');
        fbq('track', 'PageView');
    </script>


    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-P2KNTSB');
    </script>
    <!-- End Google Tag Manager -->

    {{-- Bootstrap --}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="{{ URL::to('css/styles.css') }}">

    {{-- Font Awesome --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">

    {{-- swiper css --}}
    <link rel="stylesheet" href="{{ URL::asset('css/swiper-bundle.min.css') }}">

    <link rel="stylesheet" href="{{ url('/') }}/vendor/crudbooster/assets/sweetalert/dist/sweetalert.css">

    {{-- Slick Carousel --}}
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />


    @yield('cssPage')
    <!-- Styles -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('cssonsite')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-C8QHXYBMSZ"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-C8QHXYBMSZ');
    </script>

</head>

<body>
    @include('layouts.header')
    @yield('content')
    @include('layouts.footer2')

    <script src="{{ URL::asset('js/jquery-3.4.1.min.js') }}"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
    </script>
    <script src="{{ url('/') }}/vendor/crudbooster/assets/sweetalert/dist/sweetalert.min.js"></script>
    {{-- jspdf --}}
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.3.1/jspdf.umd.min.js"></script> --}}

    <!-- Include this after the sweet alert js file -->
    @include('sweet::alert')

    @yield('js')

    <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=431618748055595&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P2KNTSB" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    {{-- <script>
     var doc = new jsPDF();
        var specialElementHandlers = {
        '#editor': function (element, renderer) {
        return true;
        }
        };

                    $('#cmd').click(function () {
                    doc.fromHTML($('#content').html(), 15, 15, {
                      'width': 170,
                      'elementHandlers': specialElementHandlers
                    });
                    doc.save('sample-file.pdf');
                  });
</script> --}}

    <script>
        $(".campaign-hero-btn").click(function() {
            $('html, body').animate({
                scrollTop: $("#campaign-title").offset().top - 150
            }, 1500);
        });
    </script>
</body>

</html>
