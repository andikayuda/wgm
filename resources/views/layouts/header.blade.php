<nav class="navbar navbar-expand-lg sticky-top">
    <a class="navbar-brand" href="{{ route('home') }}">
        <img src="{{ url('./images/logo.svg') }}" alt="">
    </a>
    <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fas fa-bars burger-btn"></i>
    </button>
  
    <a class="nav-link cart-mobile" href="{{ route('cart') }}"><i class="fas fa-shopping-cart"></i> <span class="cart-badges-mobile">{{Cart::count()}}</span> </a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="{{ route('home') }}">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('koin') }}">Koin Gatotkaca</a>
        </li>
        
        <li class="nav-item">
          <a class="nav-link" href="{{ route('campaignPage') }}">Giveaway</a>
        </li>
        <!--<li class="nav-item">-->
        <!--  <a class="nav-link wa-b" href="https://api.whatsapp.com/send/?phone=62081218975517&text=Order Koin Gatotkaca">Beli Sekarang</a>-->
        <!--</li>-->
        <li class="nav-item cart-desktop">
          <a class="nav-link" href="{{ route('cart') }}"><i class="fas fa-shopping-cart"></i><span class="cart-badges-desktop">{{Cart::count()}}</span></a>
        </li>
      </ul>
    </div>



  </nav>