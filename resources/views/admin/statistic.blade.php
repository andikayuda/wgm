@extends("crudbooster::admin_template")
@section("content")
    <div class="row bg">
        <div class="col-md-6 border-right">
            <h2>ANA</h2>
            <div class="card green" style="width: 22rem;">
              <div class="card-body">
                <h2 class="card-title">Register Data {{$ana->register}}</h2>
              </div>
            </div>
            <div class="card green" style="width: 22rem;">
              <div class="card-body">
                <h2 class="card-title">Lead {{$ana->lead}} ({{number_format((float)($ana->lead / $total) * 100, 2, '.', '')}}%)</h2>
              </div>
            </div>
            <div class="card green" style="width: 22rem;">
              <div class="card-body">
                 <h2 class="card-title">Contact {{$ana->contact}} ({{number_format((float)($ana->contact / $total) * 100, 2, '.', '')}}%)</h2>
              </div>
            </div>
            <div class="card green" style="width: 22rem;">
              <div class="card-body">
                <h2 class="card-title">Potential {{$ana->potential}} ({{number_format((float)($ana->potential / $total) * 100, 2, '.', '')}}%)</h2>
              </div>
            </div>
            <div class="card green" style="width: 22rem;">
              <div class="card-body">
                <h2 class="card-title">Win {{$ana->win}} ({{number_format((float)($ana->win / $total) * 100, 2, '.', '')}}%)</h2>
              </div>
            </div>
            <div class="card red" style="width: 22rem;">
              <div class="card-body">
                <h2 class="card-title">Lose {{$ana->lose}} ({{number_format((float)($ana->lose / $total) * 100, 2, '.', '')}}%)</h2>
              </div>
            </div>
        </div>
        <div class="col-md-6">
            <h2>MB</h2>
            <div class="card green" style="width: 22rem;">
              <div class="card-body">
                <h2 class="card-title">Register Data {{$mb->register}}</h2>
              </div>
            </div>
            <div class="card green" style="width: 22rem;">
              <div class="card-body">
                <h2 class="card-title">Lead {{$mb->lead}} ({{number_format((float)($mb->lead / $total) * 100, 2, '.', '')}}%)</h2>
              </div>
            </div>
            <div class="card green" style="width: 22rem;">
              <div class="card-body">
                <h2 class="card-title">Contact {{$mb->contact}} ({{number_format((float)($mb->contact / $total) * 100, 2, '.', '')}}%)</h2>
              </div>
            </div>
            <div class="card green" style="width: 22rem;">
              <div class="card-body">
                <h2 class="card-title">Potential {{$mb->potential}} ({{number_format((float)($mb->potential / $total) * 100, 2, '.', '')}}%)</h2>
              </div>
            </div>
            <div class="card green" style="width: 22rem;">
              <div class="card-body">
                <h2 class="card-title">Win {{$mb->win}} ({{number_format((float)($mb->win / $total) * 100, 2, '.', '')}}%)</h2>
              </div>
            </div>
            <div class="card red" style="width: 22rem;">
              <div class="card-body">
                <h2 class="card-title">Lose {{$mb->lose}} ({{number_format((float)($mb->lose / $total) * 100, 2, '.', '')}}%)</h2>
              </div>
            </div>
        </div>
    </div>
@endsection