@extends("crudbooster::admin_template")
@section('content')
    <section id="analytic" class="body container">
        <div class="text-center title-page my-5">
            <h2 style="color: #fbaa3b"><b>Analytic Page</b></h2>
        </div>
        <div style="background-color:white">
            <div class="row justify-content-space-between">
                
                <div class="col-md-6 mt-2">
                    <table class="table table-hover">
                        <thead class="table-dark">
                            <tr>
                                <th scope="col" width="80%">Nama Campaign</th>
                                <th style="text-align:right" width="20%" scope="col">Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($campaigns as $x => $ads) 
                                    <tr>
                                        <th scope="row" width="80%">{{ $ads->campaigns ? $ads->campaigns : 'No Name (NULL)' }}
                                        </th>
                                        <td width="20%" style="text-align:right">{{ $ads->campaigns_total }}</td>
                                    </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="col-md-6 mt-2">
                    <table class="table table-hover" style="table-layout: fixed;">
                        <thead class="table-dark">
                            <tr>
                                <th width="80%" scope="col" style="overflow-wrap: break-word;">Nama Medium</th>
                                <th width="20%" style="overflow-wrap: break-word;text-align:right" scope="col">Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($mediums as $x => $ads) 
                                    <tr>
                                        <th scope="row" width="80%" style="overflow-wrap: break-word;">{{ $ads->mediums ? $ads->mediums : 'No Name (NULL)' }}
                                        </th>
                                        <td width="20%" style="overflow-wrap: break-word;text-align:right">{{ $ads->mediums_total }}</td>
                                    </tr> 
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="col-md-6 mt-2">
                    <table class="table table-hover">
                        <thead class="table-dark">
                            <tr>
                                <th width="80%" scope="col">Nama Source</th>
                                <th width="20%" style="text-align:right" scope="col">Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($sources as $x => $ads) 
                                    <tr>
                                        <th scope="row" width="80%">{{ $ads->sources ? $ads->sources : 'No Name (NULL)' }}
                                        </th>
                                        <td width="20%" style="text-align:right">{{ $ads->sources_total }}</td>
                                    </tr> 
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="col-md-6 mt-2">
                    <table class="table table-hover">
                        <thead class="table-dark">
                            <tr>
                                <th width="80%" scope="col">Nama Content</th>
                                <th width="20%" style="text-align:right" scope="col">Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contents as $x => $ads) 
                                    <tr>
                                        <th scope="row" width="80%">{{ $ads->contents ? $ads->contents : 'No Name (NULL)' }}
                                        </th>
                                        <td width="20%" style="text-align:right">{{ $ads->contents_total }}</td>
                                    </tr> 
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection