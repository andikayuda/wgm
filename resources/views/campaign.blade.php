@extends('layouts.app')

@section('content')
    <div>
        {{-- BANNER SECTION --}}
        <div class="campaign-hero">
            <div class="campaign-marquee">
                <div class="campaign-hero-txt">
                    <!--<a href="javascript:void(0)" class="campaign-hero-btn">Isi Form</a>-->
                </div>
            </div>
        </div>

        <div class="container">
          <h1 id="campaign-title" class="campaign-title"><span>Isi Data</span> Berikut</h1>

          <form id="form-input">
            @csrf
            <div class="form-group">
              <input type="text" class="form-control redeem-input" autocomplete="off" id="namaLengkap" name="nama" placeholder="Nama Lengkap" required>
            </div>
            <div class="form-group">
              <input type="text" class="form-control redeem-input" autocomplete="off" id="nomorWa" name="hp" placeholder="Nomor Whatsapp" required>
            </div>
            <div class="form-group">
              <input type="email" class="form-control redeem-input" autocomplete="off" id="alamatEmail" name="email" placeholder="Alamat e-mail" required>
            </div>
            <div class="form-group">
              <textarea class="form-control redeem-input" autocomplete="off" id="alamat-lengkap" rows="6" name="alamat" placeholder="Alamat Lengkap" required></textarea>
            </div>
            {{-- <div class="form-group">
              <input type="number" min="0" maxlength="5" class="form-control redeem-input" id="jumlahKoinDikirim" name="jumlah" placeholder="Berapa jumlah Koin yang ingin dikirimkan?" required>
            </div> --}}

            <h5 class="giveaway-prize-title">Pilih Hadiah Giveaway</h5>

            <div class="form-group row mb-3 align-items-center giveaway-prize">
              <label for="inputPassword" class="col-sm-6 col-form-label">1 unit iPhone 12 Pro</label>
              <div class="col-sm-6">
                <input type="number" class="form-control total" id="iPhone" min='0' placeholder="Jumlah Koin" style="width: 100%; height: 50px">
              </div>
            </div>
            <div class="form-group row mb-3 align-items-center giveaway-prize">
              <label for="inputPassword" class="col-sm-6 col-form-label">1 unit Playstation 5</label>
              <div class="col-sm-6">
                <input type="number" class="form-control total" id="PS5" min='0' placeholder="Jumlah Koin" style="width: 100%; height: 50px">
              </div>
            </div>
            <div class="form-group row mb-3 align-items-center giveaway-prize">
              <label for="inputPassword" class="col-sm-6 col-form-label">1 unit Sepeda Gunung MTB United Detroit SV 2020 24" 21SP</label>
              <div class="col-sm-6">
                <input type="number" class="form-control total" id="Sepeda" min='0' placeholder="Jumlah Koin" style="width: 100%; height: 50px">
              </div>
            </div>
            <div class="form-group row mb-3 align-items-center giveaway-prize">
              <label for="inputPassword" class="col-sm-6 col-form-label">1 unit HP Realme 5 Pro</label>
              <div class="col-sm-6">
                <input type="number" class="form-control total" id="Realme" min='0' placeholder="Jumlah Koin" style="width: 100%; height: 50px">
              </div>
            </div>
            <div class="form-group row mb-3 align-items-center giveaway-prize">
              <label for="inputPassword" class="col-sm-6 col-form-label">Voucher Diamond (Freefire/Mobile Legend) 1.000.000,-</label>
              <div class="col-sm-6">
                <input type="number" class="form-control total" id="Diamond" min='0' placeholder="Jumlah Koin" style="width: 100%; height: 50px">
              </div>
            </div>

            <div class="jumlah-koin">
              <h5>Jumlah Koin yang dikirim</h5>
              <h5 id="totalKoin">0</h5>
            </div>

            <div class="text-center">
                    <!--<button class="btn btn-redeem-form" disabled>SUBMIT</button>-->
                <!--@if (((int) \Carbon\Carbon::now()->format('H')) > 23 && ((int) \Carbon\Carbon::now()->format('i')) > 59 || ((int) \Carbon\Carbon::now()->format('d')) > 20)-->
                <!--@else-->
                <!--    <button class="btn btn-redeem-form" id="submit-form" type="submit">SUBMIT</button>-->
                <!--@endif-->
            </div>
          </form>
  
          {{-- <div class="pad margin no-print">
            <div class="callout callout-info" style="margin-bottom: 0!important;">
              <h4><i class="fa fa-info"></i> Note:</h4>
              This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
            </div>
          </div> --}}

        
        <div id="form-download" class="container">
          
        </div>

      </div>
</div>

@endsection

@section('js')
    <script>
      
    </script>

    <script>
      $('.form-group').on('input', '.total', function() {
        var totalSum = 0;
        $('.form-group .total').each(function() {
          var inputVal = $(this).val();
          if ($.isNumeric(inputVal)) {
            totalSum += parseFloat(inputVal)
          }
        })
        $('#totalKoin').text(totalSum)
      })

      jQuery(document).ready(function(){
            jQuery('#submit-form').click(function(e){
              var totalKoin = $( "#totalKoin" ).text()
               e.preventDefault();
               $.ajaxSetup({
                  headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                  }
              });
               jQuery.ajax({
                  url: "{{ route('submitCampaign') }}",
                  method: 'post',
                  data: {
                     nama: jQuery('#namaLengkap').val(),
                     hp: jQuery('#nomorWa').val(),
                     email: jQuery('#alamatEmail').val(),
                     alamat: jQuery('#alamat-lengkap').val(),
                     jumlah: totalKoin,
                     koinIphone: jQuery('#iPhone').val() || 0,
                     koinPs5: jQuery('#PS5').val() || 0,
                     koinHp: jQuery('#Realme').val() || 0,
                     koinDiamond: jQuery('#Diamond').val() || 0,
                     koinSepeda: jQuery('#Sepeda').val() || 0
                  },
                  success: function(result){
                    // console.log(result)

                    let coin = [
                          {name: 'PS5', qty: result.jumlahKoinPs5},
                          {name: 'iPhone', qty: result.jumlahKoinIphone},
                          {name: 'Realme', qty: result.jumlahKoinHp},
                          {name: 'Diamond', qty: result.jumlahKoinDiamond},
                          {name: 'Sepeda', qty: result.jumlahKoinSepeda},
                        ]

                    var isiTable = ''
                    

                    coin.forEach(element => {
                      // console.log(element)
                      if (element.qty !== "0") {
                        isiTable += `
                          <tr>
                            <td>Koin Gatotkaca - ${element.name}</td>
                            <td>${element.qty}</td>
                          </tr>
                        `
                      }
                        
                    });
                    
                    

                    let codeBlock = `
                    <h5 class="text-center my-5">Silahkan print resi ini di paket koin yang akan Anda kirimkan. <br>
                    Jika kesulitan dalam print resi, harap <strong>ditulis tangan dengan format yang sama.</strong></h5>
                    <div style="border: 1px solid black; padding-left: 25px;padding-right: 25px">
                    <!-- Main content -->
                    <section class="invoice">
                      <!-- title row -->
                      <div class="row">
                        <div class="col-xs-12">
                          <h2 class="page-header">
                            <img src="{{ asset('./images/giveaway/wgm-black.png') }}" height="300px">
            
                          </h2>
                        </div>
                      </div>
                      <!-- info row -->
                      <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          Dari
                          <address>
                            <p><strong>${result.nama}</strong></p>
                            <p>${result.alamat}</p>
                            <p><strong>Phone:</strong> ${result.noWhatsapp}</p>
                          </address>
                          
                  
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          To
                          <address>
                            <strong>Warisan Gajahmada</strong><br>
                            Ruko Crystal 8 No. 18 Alam Sutera <br>
                            Pakualam, Kec. Serpong Utara, Kota Tangerang Selatan,<br>
                            Banten 15320 <br> <br>
                            No. Telp: 0812 1897 5517
                          </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          Detail Transaksi
                            <p><b>No Resi : ${result.kode_form} </b></p>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                  
                      <!-- Table row -->
                      <div class="row">
                        <div class="col-xs-12 table-responsive">
                          <table class="table table-striped">
                            <thead>
                            <tr>
                              <th>Product</th>
                              <th>Qty</th>
                            </tr>
                            </thead>
                            <tbody>
                              
                              ${isiTable}
                            <tr>
                              <td> <strong>Total Koin</strong> </td>
                              <td> ${result.jumlahKoinTotal} </td>
                            </tr>
                              
                  
                            </tbody>
                          </table>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                  
                      
                    </section>
                  </div>
                  <!-- this row will not appear when printing -->
                  <div class="row no-print justify-content-center my-5" style="gap: 25px">
                    <div class="col-xs-6">
                      {{-- <a href="#" target="_blank" class="btn print-btn">Print</a> --}}
                      <span onclick="window.print();" class="btn print-btn">Print</span>
                      
                    </div>
                    <div class="col-xs-6">
                      <a href="pdf-form/`+result.id+`" download class="btn download-btn">
                        Download Resi
                      </a>
                    </div>
                  </div>
                    <h5 class="text-center my-5" style="font-weight: 700">Setelah mengirimkan koin, harap menunggu <br> konfirmasi kupon undian melalui email</h5>
                </div>
                    `



                    
                    // console.log(result)
                    $("#form-input").addClass("d-none");
                    $("#campaign-title").addClass("d-none");

                    document.getElementById("form-download").innerHTML = codeBlock

                    swal("", "Berhasil Submit Form", "success");
                  }});
               });
            });
    </script>
@endsection