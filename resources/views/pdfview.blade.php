<html>
  <head>
      {{-- FAVICON --}}
      <link rel="icon" type="image/x-icon" href="{{ url('images/favicon/favicon.ico') }}">

      <title>Warisan Gajahmada</title>

      {{-- Bootstrap --}}
      {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> --}}
       {{-- Font Awesome --}}
       {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"> --}}
      <!-- CSS -->
      {{-- <link rel="stylesheet" media="all" href="{{ asset('css/styles.css')}}"> --}}
      {{-- <link rel="stylesheet" href="{{url('/')}}/vendor/crudbooster/assets/sweetalert/dist/sweetalert.css"> --}}
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">-->
      <!-- Styles -->
            <link rel="stylesheet"  href="{{ public_path('css/bootstrap.css') }}">

      <style>
        
      </style>
  </head>
  <body>
    <img src="https://gudang.warisangajahmada.com/images/ced-logo-grey.png" height="150px">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          {{-- <img src="https://gudang.warisangajahmada.com/images/ced-logo-grey.png" height="150px"> --}}
          {{-- <img class="pull-right"  src="{{ CRUDBooster::getSetting('logo_partner')?asset(CRUDBooster::getSetting('logo_partner')):asset('vendor/crudbooster/assets/logo_crudbooster.png') }}" height="100px"> --}}
        </h2>
      </div>
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col" style="float:left; width:33%">
        Dari
        <address>
          <p><strong>{{$data}}</strong></p>
          <p>{{$alamat}}</p>
          <p><strong>Phone:</strong> {{$noWhatsapp}}</p>
        </address>
        

      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col" style="float:left; width:33%">
        To
        <address>
            <strong>Warisan Gajahmada</strong><br>
            Ruko Crystal 8 No. 18 Alam Sutera <br>
            Pakualam, Kec. Serpong Utara, Kota Tangerang Selatan,<br>
            Banten 15320 <br> <br>
            No. Telp: 0812 1897 5517
          </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col" style="float:left; width:33%">
        Detail Transaksi
          <p><b>No Invoice : {{$kode_form}} </b></p>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <div style="clear:both"></div>

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <th>Product</th>
            <th>Qty</th>
          </tr>
          </thead>
          <tbody>
            @foreach ($listKupon as $item=>$key)
                @if ($key != 0)
                    <tr>
                        <td>Koin Gatotkaca - {{$item}}</td>
                        <td>{{$key}}</td>
                    </tr>
                @endif
            @endforeach
            
          <tr>

            <td>Koin Gatotkaca</td>
            <td>{{$jumlahKoinTotal}}</td>
          </tr>
            

          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    
    {{-- <a href="{{ route('pdfForm') }}">invoice</a> --}}
  </body>
</html>