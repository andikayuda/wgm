<div class="d-flex justify-content-center mb-6">
    <div class="comment-box">

        <img src="https://unper.ac.id/wp-content/uploads/2016/08/dummy-prod-1.jpg" alt="" class="img-comment">

        <div class="">
            <div class="comment-name">
                <h4 class="title-part mb-0" style="text-align: start">
                    {{ $name }}
                </h4>
                <p class="date-comment mb-0">{{ $date }}</p>
            </div>

            <p class="detail-data">
                {{ $detail }}
            </p>
        </div>
    </div>
</div>
