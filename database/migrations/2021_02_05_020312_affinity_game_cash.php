<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AffinityGameCash extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affinityGame', function (Blueprint $table) {
            //
            $table->bigIncrements('id');
            $table->bigInteger('affinityId');
            $table->string('prize');
            $table->text('imageMob');
            $table->text('imageWeb');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('affinityGame', function (Blueprint $table) {
            //
        });
    }
}
